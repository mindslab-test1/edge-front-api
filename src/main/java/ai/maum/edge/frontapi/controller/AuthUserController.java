package ai.maum.edge.frontapi.controller;

import java.util.UUID;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ai.maum.edge.frontapi.commons.payload.ApiResponse;
import ai.maum.edge.frontapi.commons.security.CurrentUser;
import ai.maum.edge.frontapi.commons.security.JwtTokenProvider;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.commons.utils.CookieUtil;
import ai.maum.edge.frontapi.commons.utils.RedisUtil;
import ai.maum.edge.frontapi.model.TokenVO;
import ai.maum.edge.frontapi.model.dto.JwtTokenDto;
import ai.maum.edge.frontapi.model.dto.LoginDto;
import ai.maum.edge.frontapi.model.entity.User;
import ai.maum.edge.frontapi.service.UserEdgeService;
import ai.maum.edge.frontapi.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ai.maum.edge.frontapi.service.AuthService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/auth/user")
public class AuthUserController {


    private final AuthService authSvc;

    private final UserService userSvc;
    
    @Autowired
    private final UserEdgeService userEdgeService;
    
    @Autowired
    private RedisUtil redisUtil;
    
    @Autowired
    private CookieUtil cookieUtil;
    
    @Autowired 
    private JwtTokenProvider tokenProvider;

    private final ModelMapper modelMapper = new ModelMapper();

    @Value("${sso.callback}")
    String OAUTH_CALLBACK_URI;
    
    @Value("${app.jwtRefreshExpriationInMs}")
    long jwtRefreshExpriationInMs;

//    @PostMapping("/signup")
//    public ResponseEntity<ApiResponse> userSignup(@RequestBody @Validated UserSignupDto dto)
//    {
//        String logTitle = "userSignup/";
//
//        try {
//            if(userSvc.doesUserExist(dto.getEmail())) {
//                log.info(logTitle + "Email[{}], [Already exists]", dto.getEmail());
//                return new ResponseEntity<>(new ApiResponse(true, "이미 가입"), HttpStatus.OK);
//            }
//
//            Role userRole = userSvc.getRole(RoleName.ROLE_USER);
//            User user = modelMapper.map(dto, User.class);
//
//            user = userSvc.signup(user, userRole);
//
//            log.info(logTitle + "Email[{}], UserNo[{}], [Success]", user.getEmail(), user.getId());
//            return new ResponseEntity<>(new ApiResponse(true, "가입 성공"), HttpStatus.CREATED);
//        }
//        catch (Exception e) {
//            log.warn(logTitle + "Exception=" + StackTrace.getStackTrace(e));
//            return new ResponseEntity<>(new ApiResponse(false, "가입 실패"), HttpStatus.BAD_REQUEST);
//        }
//    }


    @ApiOperation(value = "로그인")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dto", value = "로그인 정보", dataType = "LoginDto")
    })
    @PostMapping("/login")
    public ResponseEntity<?> login(HttpServletRequest request, @RequestBody LoginDto dto)
    {
        String logTitle = "login/Id[" + dto.getId() + "], Password[" + dto.getPassword() + "], ";

        Authentication auth;
        String jwt;
        try {
            auth = authSvc.authenticate(dto.getId(), dto.getPassword());
            UserPrincipal userPrincipal = (UserPrincipal) auth.getPrincipal();
            jwt = authSvc.generateJwtToken(userPrincipal, null);
        }
        catch (Exception e) {
            log.warn(logTitle + e.getMessage());
            return new ResponseEntity<>(new ApiResponse(false, "로그인에 실패하였습니다.", null), HttpStatus.BAD_REQUEST);
        }

        log.info(logTitle + "[Success]", dto.getId());
        return ResponseEntity.ok(new JwtTokenDto(jwt, auth.getAuthorities()));
    }

    @GetMapping(value = "/callback")
    public ResponseEntity<?> authCallback(HttpServletRequest request,
                               HttpServletResponse response,
                               @RequestParam("code") String code,
                               @RequestParam("state") String state) throws Exception {

        String logTitle = "authCallback/code[" + code + "], state[" + state + "], ";

        Authentication auth;
        String jwt;
        String refreshToken;
        
        //중복 로그인 방지
        String uuid = UUID.randomUUID().toString();
        
        TokenVO token = authSvc.getAuthToken(code, OAUTH_CALLBACK_URI);
        token.setUuid(uuid);
        User user = authSvc.getUserInfo(token);
        
        if(user != null) {
            // JWT 생성
            try {
                auth = authSvc.authenticate(user.getEmail(), user.getPassword());
                
                UserPrincipal userPrincipal = (UserPrincipal) auth.getPrincipal();
                
                if (user.getId() == null || user.getId() == 0) {
                	((UserPrincipal) auth.getPrincipal()).setUserNo((long) 0);
                	((UserPrincipal) auth.getPrincipal()).setName(user.getName());
                } else {
                    //refreshToken 저장
                	refreshToken = authSvc.refreshGenerateToken(userPrincipal, token);
                	redisUtil.setData("userId_" + user.getId(), uuid);
                	
                	int maxAge = (int) (jwtRefreshExpriationInMs/1000);
                	
                	Cookie cookie = cookieUtil.setCookie(CookieUtil.REFRESH_TOKEN, refreshToken, maxAge);
                	response.addCookie(cookie);
                	
                	redisUtil.setData(CookieUtil.REFRESH_TOKEN + "_" + user.getId(), refreshToken);
                	
                }
                
                jwt = authSvc.generateJwtToken(userPrincipal, token);

                log.info(logTitle + "[Success]", user.toString());
                return ResponseEntity.ok(new JwtTokenDto(jwt, auth.getAuthorities()));
            } catch (Exception e) {
                log.warn(logTitle + e.getMessage());
                return new ResponseEntity<>(new ApiResponse(false, "로그인에 실패하였습니다.", null), HttpStatus.BAD_REQUEST);
            }
        }
        else {
            log.warn(logTitle + "[Fail]");
            return new ResponseEntity<>(new ApiResponse(false, "로그인에 실패하였습니다.", null), HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value = "로그아웃")
    @PostMapping("/logout")
    public ResponseEntity<?> logout(HttpServletRequest request, HttpServletResponse response)
    {
        //String logTitle = "logout/ID[" + user.getUserNo() + "], ";
        
        //캐시 삭제
        //userEdgeService.removeCache(user.getUserNo());
        
        //refresh 쿠키 삭제
        Cookie cookie = cookieUtil.getCookie(request, CookieUtil.REFRESH_TOKEN);
        
        if (cookie != null) {
        	cookie.setMaxAge(0);
            cookie.setPath("/"); 
            response.addCookie(cookie);
        }
        
        
        try {
            //log.info(logTitle + "[Logout]");
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch (Exception e) {
            //log.warn(logTitle + StackTrace.getStackTrace(e));
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
    
    @ApiOperation(value = "토큰 재인증")
    @GetMapping("/republishToken")
    public ResponseEntity<?> republishToken(HttpServletRequest request, HttpServletResponse response, @CurrentUser UserPrincipal user)
    {
    	
    	Cookie accessToken = cookieUtil.getCookie(request, "userToken");
    	
    	if (accessToken != null && tokenProvider.validateToken(accessToken.getValue())) {
    		String jwt = accessToken.getValue();
    		return ResponseEntity.ok(new JwtTokenDto(jwt, user.getAuthorities()));
    	} else {
    		return new ResponseEntity<>(false, HttpStatus.UNAUTHORIZED);    		
    	}
    	
    }


    @ApiOperation(value = "로그인 체크")
    @GetMapping("/ping")
    @PreAuthorize("hasAnyRole('ENGINEER', 'USER', 'ADMIN', 'COMP_ADMIN')")
    public ResponseEntity<?> ping(@CurrentUser UserPrincipal user)
    {
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
    
}
