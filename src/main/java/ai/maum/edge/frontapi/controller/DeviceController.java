package ai.maum.edge.frontapi.controller;

import ai.maum.edge.frontapi.model.dto.DeviceDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ai.maum.edge.frontapi.service.DeviceService;
import io.swagger.annotations.ApiOperation;

/**
 * DeviceController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-22  / 이성주	 / 최초 생성
 * </pre>
 * @since 2020-01-22
 */
@RestController
@RequestMapping(value = "/api/device")
public class DeviceController {

    final DeviceService deviceService;
    
    

    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }
    
    @ApiOperation(value = "Edge Id 중복 체크")
    @PostMapping("/duplicate")
    public ResponseEntity<?> checkEdgeId(@RequestBody DeviceDto dto) {
    	
    	try {
    		boolean isDup = deviceService.checkEdgeId(dto);
    		return new ResponseEntity<>(isDup, HttpStatus.OK);
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    	}
    	
    }
    
    @ApiOperation(value = "Edge Road 재시작")
    @GetMapping("/{deviceId}/restart")
    public ResponseEntity<?> restartEdgeRoad(@PathVariable Long deviceId) {
    	
    	try {
    		String result = deviceService.restartEdge(deviceId);
    		return new ResponseEntity<>(result, HttpStatus.OK);
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    	}
    	
    }
    
    @ApiOperation(value = "Edge Road 정보")
    @GetMapping("/{deviceId}/info")
    public ResponseEntity<?> getEdgeInfo(@PathVariable Long deviceId) {
    	
    	
    	try {
    		DeviceDto.info info = deviceService.getEdgeInfo(deviceId);
    		return new ResponseEntity<>(info, HttpStatus.OK);
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
    	
    }
    
}
