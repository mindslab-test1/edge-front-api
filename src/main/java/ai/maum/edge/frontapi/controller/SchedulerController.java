package ai.maum.edge.frontapi.controller;

import java.io.File;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import ai.maum.edge.frontapi.commons.utils.LocalDateParser;
import ai.maum.edge.frontapi.commons.utils.ModelMapperService;
import ai.maum.edge.frontapi.model.entity.RoadRecog;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ai.maum.edge.frontapi.service.RoadRecogService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class SchedulerController {
	
	private final RoadRecogService roadRecogService;
	
	private final ModelMapperService modelMapperService;
	
	private final EntityManager entityManager;
	
	private int size = 2000;
	
	private int term = 7;
	
	@Value("${recog.path}")
	private String rootDir;
	
	Logger logger = LoggerFactory.getLogger(SchedulerController.class);
	
	//매일 새벽 3시 실행
	@ApiOperation(value = "차량 인식 테이블 백업(DB)")
	@Scheduled(cron = "0 0 3 * * ?")
	@Transactional
	public void roadRecogBackupScheduler() throws Exception {
		
		logger.info("roadRecogBackupScheduler 시작 : " + LocalDateTime.now());
		
		int result = roadRecogService.callRoadRecogBackupProc();
		
		if (result <= 0) {
			throw new Exception("삭제 중 오류 발생");
		}
		
		
		logger.info("roadRecogBackupScheduler 종료 : " + LocalDateTime.now());
		
	}
	
	@ApiOperation(value = "차량 인식 테이블 백업(EXCEL)")
	@Transactional
	public void roadRecogBackupFileByExcel() throws Exception {
		LocalDateTime startTime = LocalDateTime.now();
		
		logger.info("roadRecogBackupFileByExcel 시작 : " + startTime);
		
		String[] headerList = {"recogSeq", "edgeNodeSeq", "passTime", "editTime", "carNumber", "carType", "carImgPath", "plateImgPath", "direction", "confLevel"
							   ,"lane", "speed", "deviceId", "edgeId", "addr", "addrDetail", "cameraDir", "clientCompanyId", "clientCompanyName", "cameraNo", "userId"
							   ,"modId", "modName", "requestJson"};
		
		LocalDate date = LocalDate.now().minusDays(term);
		String passTime = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		
		List<String> dateList = roadRecogService.roadRecogBackupDateList(passTime);
		
		for(String dates : dateList) {
			LocalDateParser parser = new LocalDateParser(dates, "yyyyMMdd");
			String fileName = "roadRecog_" + dates + ".xlsx";
			int datePath = parser.getDate().getMonthValue();
			String pathDir = rootDir + File.separator + parser.getDate().getYear() + File.separator + ((datePath >= 10) ? datePath : "0" + datePath);
			
			Long cnt = roadRecogService.getBackupRoadRecogCount(entityManager, parser);
			
			Path path = Paths.get(pathDir, fileName);
			
			int fileIdx = 1;
			while (true) {
				if (!Files.exists(path)) break;
				fileName = "roadRecog_" + dates + "_" + ((fileIdx >= 10) ? fileIdx : "0" + fileIdx) + ".xlsx";
				path = Paths.get(pathDir, fileName);
			}
			
			Files.createDirectories(path.getParent());
			boolean isFile = Files.exists(path);
			
			OutputStream out = null;
			SXSSFWorkbook wb = new SXSSFWorkbook(-1);
			Sheet sh = wb.createSheet();
			Row header = sh.createRow(0);
			
			for (int j = 0; j < headerList.length; j++) {
				header.createCell(j).setCellValue(headerList[j]);				
			}
			
			int rowNum = 0;
			
			int pageSize =  (int) Math.ceil(cnt/(double)size);
			
			for (int i = 0; i < pageSize; i++) {
				
				org.springframework.data.domain.PageRequest pageable = org.springframework.data.domain.PageRequest.of(i, size);
				
				List<RoadRecog> recogList = roadRecogService.getBackupRoadRecogList(entityManager, parser, pageable);
				
				for (RoadRecog roadRecog : recogList) {
					
					Row row = sh.createRow(rowNum + 1);
					Map<String, String> recogMap = this.ConverObjectToMap(roadRecog);
					
					for (int j = 0; j < headerList.length; j++) {
						row.createCell(j).setCellValue(String.valueOf(recogMap.get(headerList[j])));
					}
					
					rowNum++;
				}
				
			}
			try {
				
				out = Files.newOutputStream(path);
				
				wb.write(out);
				
			} catch (Exception e) {
				
			} finally {
				if (out != null) {
					out.close();					
				}
				
				if (wb != null) {
					wb.close();
				}
				
			}
			
			Long delResult = roadRecogService.deleteRoadRecog(entityManager, parser);
			
			if (delResult <= 0) {
				throw new Exception("roadRecogBackupFile 삭제 오류 발생");
			}
			
		}
		
		LocalDateTime endTime = LocalDateTime.now();
		Duration duration = Duration.between(startTime, endTime);
		
		logger.info("roadRecogBackupFileByExcel 종료 : " + endTime);
		logger.info("걸린 시간(초) : " + duration.getSeconds() + "초");
	}
	
	@ApiOperation(value = "차량 인식 테이블 백업(LOG)")
	@Transactional
	public void roadRecogBackupFileByLog() throws Exception {
		LocalDateTime startTime = LocalDateTime.now();
		
		logger.info("roadRecogBackupFileByLog 시작 : " + startTime);
		
		LocalDate date = LocalDate.now().minusDays(term);
		String passTime = date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		
		List<String> dateList = roadRecogService.roadRecogBackupDateList(passTime);
		
		for(String dates : dateList) {
			LocalDateParser parser = new LocalDateParser(dates, "yyyyMMdd");
			String fileName = "roadRecog_" + dates + ".log";
			int datePath = parser.getDate().getMonthValue();
			String pathDir = rootDir + File.separator + parser.getDate().getYear() + File.separator + ((datePath >= 10) ? datePath : "0" + datePath);
			
			Long cnt = roadRecogService.getBackupRoadRecogCount(entityManager, parser);
			
			Path path = Paths.get(pathDir, fileName);
			
			FileChannel channel = FileChannel.open(path, StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND);
			
			int pageSize =  (int) Math.ceil(cnt/(double)size);
			
			for (int i = 0; i < pageSize; i++) {
				
				org.springframework.data.domain.PageRequest pageable = org.springframework.data.domain.PageRequest.of(i, size);
				
				List<RoadRecog> recogList = roadRecogService.getBackupRoadRecogList(entityManager, parser, pageable);
				
				for (RoadRecog roadRecog : recogList) {
					
					String txt = roadRecog.toString() + "\n";
					
					ByteBuffer buffer = ByteBuffer.allocateDirect(txt.length());
					Charset charset = Charset.forName("UTF-8");
					buffer = charset.encode(txt);
					
					channel.write(buffer);

				}
				
			}
			
			channel.close();
			
			Long delResult = roadRecogService.deleteRoadRecog(entityManager, parser);
			
			if (delResult <= 0) {
				throw new Exception("roadRecogBackupFile 삭제 오류 발생");
			}
			
			
		}
		
		LocalDateTime endTime = LocalDateTime.now();
		Duration duration = Duration.between(startTime, endTime);
		
		logger.info("roadRecogBackupFileByLog 종료 : " + endTime);
		logger.info("걸린 시간(초) : " + duration.getSeconds() + "초");
	}
	
	
	public Map<String, String> ConverObjectToMap(Object obj) throws Exception {
		Map<String, String> resultMap = new HashMap<String, String>();
		
		for (Field field : obj.getClass().getDeclaredFields()) {
			field.setAccessible(true);
			resultMap.put(field.getName(), String.valueOf(field.get(obj)));
		}
		
		return resultMap;
	}
}
