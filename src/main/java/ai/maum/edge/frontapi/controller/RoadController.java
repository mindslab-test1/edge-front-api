package ai.maum.edge.frontapi.controller;

import java.util.List;
import java.util.Map;

import ai.maum.edge.frontapi.commons.payload.ApiResponse;
import ai.maum.edge.frontapi.commons.security.CurrentUser;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.model.dto.DeviceDto;
import ai.maum.edge.frontapi.model.dto.DeviceSearchDto;
import ai.maum.edge.frontapi.model.dto.RealTimeSearchDto;
import ai.maum.edge.frontapi.model.dto.RoadDto;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ai.maum.edge.frontapi.repository.RecentlyRoadRepository;
import ai.maum.edge.frontapi.service.DeviceService;
import ai.maum.edge.frontapi.service.RoadService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;

/**
 * DeviceController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-22  / 이성주	 / 최초 생성
 * </pre>
 * @since 2020-01-22
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api/road")
public class RoadController {

    private final RoadService roadService;
    
    private final DeviceService deviceService;
    
    private final RecentlyRoadRepository recentlyRoadRepository;
    
    @ApiOperation(value = "Edge Road 조회")
    @GetMapping
    public ResponseEntity<?> getEdgeRoadList(@CurrentUser UserPrincipal user, DeviceSearchDto deviceSearchDto) {
    	
    	try {
    		Page<DeviceDto.list> list = roadService.getEdgeRoadList(deviceSearchDto, user);
    		return new ResponseEntity<>(list, HttpStatus.OK);
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST); 
		}
    	
    }
    
    @ApiOperation(value = "Edge Road 등록")
    @PostMapping("/admin")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> addEdgeRoad(@CurrentUser UserPrincipal user, @RequestBody DeviceDto.RoadList dto) {
    	
    	int result = roadService.addEdgeRoad(dto, user.getUserNo());
    	
    	if (result == 1) {
    		return new ResponseEntity<>(true, HttpStatus.OK);
    	} else if (result == -1) {
    		return new ResponseEntity<>(false, HttpStatus.OK);
    	} else {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);      		
    	}
    	
    }
    
    
    @ApiOperation(value = "Edge Road 상세")
    @GetMapping("/{deviceId}")
    public ResponseEntity<?> detailEdgeRoad(@PathVariable Long deviceId) {
    	
    	try {
    		DeviceDto.RoadList list = roadService.detailEdgeRoad(deviceId);
    		return new ResponseEntity<>(list, HttpStatus.OK);
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
    	
    }
    
    @ApiOperation(value = "Edge Road 수정")
    @PutMapping("/admin/{deviceId}")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> editEdgeRoad(@CurrentUser UserPrincipal user, @PathVariable Long deviceId, @RequestBody DeviceDto.RoadList dto) {
    	
    	try {
    		
    		boolean isEdit = roadService.editEdgeRoad(dto, user);
    		return new ResponseEntity<>(isEdit, HttpStatus.OK);
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
    	
    }
    
    @ApiOperation(value = "Edge Road 삭제")
    @DeleteMapping("/admin")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<?> delEdgeRoad(@CurrentUser UserPrincipal user, @RequestBody List<Long> deviceIds) {
    	
    	try {
    		
    		boolean isEdit = roadService.delEdgeRoad(deviceIds, user.getUserNo());
    		return new ResponseEntity<>(isEdit, HttpStatus.OK);
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
    	
    }
    
    @ApiOperation(value = "Edge Road api 정보")
    @PostMapping("/refresh")
    public ResponseEntity<?> refreshEdgeRoadInfo(@RequestBody List<DeviceDto.list> dtoList) {
    	
    	try {
    		
    		List<DeviceDto.list> list = roadService.refreshEdgeRoadInfo(dtoList);
    		return new ResponseEntity<>(list, HttpStatus.OK);
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
    	
    }
    
    @ApiOperation(value = "최근 edge Road 조회")
    @GetMapping("/recentlyEdge/{edgeId}")
    public ResponseEntity<?> recentlyEdge(@PathVariable String edgeId) {
    	
    	return new ResponseEntity<>(recentlyRoadRepository.findByEdgeId(edgeId), HttpStatus.OK);
    }
    
    @ApiOperation(value = "Edge Road 정책 정보 조회")
    @GetMapping("/policy/{edgeNodeSeq}")
    public ResponseEntity<?> edgeRoadPolicyDetail(@PathVariable Long edgeNodeSeq) {
    	
    	try {
    		
    		RoadDto.Policy policy = roadService.edgeRoadPolicyDetail(edgeNodeSeq);
    		
    		return new ResponseEntity<>(policy, HttpStatus.OK);    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
    	
    }
    
    @ApiOperation(value = "Edge Road 정책 정보 수정")
    @PutMapping("/policy/{edgeNodeSeq}")
    public ResponseEntity<?> edgeRoadPolicyEdit(@CurrentUser UserPrincipal user, @PathVariable Long edgeNodeSeq, @RequestBody RoadDto.Policy dto) {
    	
    	try {
    		
    		boolean isUpdate = roadService.edgeRoadPolicyEdit(user.getUserNo(), dto);
    		
    		return new ResponseEntity<>(isUpdate, HttpStatus.OK);    		
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
    	
    }
    
    @ApiOperation(value = "Edge Road 실시간 차량 인식")
    @PostMapping("/real")
    public ResponseEntity<?> edgeRoadRealTime(@CurrentUser UserPrincipal user, @RequestBody RealTimeSearchDto dto) {
    	
    	try {
    		
    		ApiResponse response = roadService.edgeRoadRealTime(user, dto);
    		
    		return new ResponseEntity<>(response, HttpStatus.OK);    		
    	} catch (Exception e) {
    		e.printStackTrace();
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
    	
    }
    
    @ApiOperation(value = "Edge Road 실시간 차량 인식 주소 목록 조회")
    @PostMapping("/addrList")
    public ResponseEntity<?> edgeRoadRealAddrList(@CurrentUser UserPrincipal user, @RequestBody RealTimeSearchDto dto) {
    	
    	try {
    		
    		Map<String, Object> result = roadService.edgeRoadRealAddrList(user,dto);
    		
    		return new ResponseEntity<>(result, HttpStatus.OK);    		
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
    	
    }
    
    @ApiOperation(value = "Edge Road 실시간 차로 정보 조회")
    @GetMapping("/recentlyEdgeLine/{edgeId}")
    public ResponseEntity<?> recentlyEdgeRoadInfo(@PathVariable String edgeId) {
    	
    	try {
    		
    		List<Map<String, Object>> list = roadService.recentlyEdgeRoadInfo(edgeId);
    		return new ResponseEntity<>(list, HttpStatus.OK);
    	} catch (Exception e) {
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
    	
    }
}
