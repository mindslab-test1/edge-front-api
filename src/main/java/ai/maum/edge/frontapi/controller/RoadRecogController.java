package ai.maum.edge.frontapi.controller;

import java.util.Map;

import ai.maum.edge.frontapi.commons.security.CurrentUser;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.model.dto.RoadRecogDto;
import ai.maum.edge.frontapi.model.dto.RoadRecogSearchDto;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ai.maum.edge.frontapi.service.RoadRecogService;
import ai.maum.edge.frontapi.service.UserEdgeService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * RoadRecogController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-01-21  / 차승호	 / 최초 생성
 * </pre>
 * @since 2020-12-21
 */
@Slf4j
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/road/recog")
public class RoadRecogController {
	
    final UserEdgeService userEdgeService;
    final RoadRecogService roadRecogService;
    
    public RoadRecogController(UserEdgeService userEdgeService, RoadRecogService roadRecogService) {
        this.userEdgeService = userEdgeService;
        this.roadRecogService = roadRecogService;
    }

    /**
     * 차량 인식 결과 조회
     * @param roadRecogServiceRequest
     * @return
     **/
    @ApiOperation(value = "차량 인식 결과 조회")
    @PostMapping("/list")
    public ResponseEntity<?> getRoadRecog(@CurrentUser UserPrincipal user, @RequestBody RoadRecogSearchDto roadRecogSearchDto) {
    //public ResponseEntity<?> getRoadRecog(@RequestBody RoadRecogSearchDto roadRecogSearchDto) {
    	log.info("/getRoadRecog["+user.getUserNo()+"]");

    	try {
    		Page<RoadRecogDto> list = roadRecogService.getRoadRecogList(user, roadRecogSearchDto);
            return new ResponseEntity<>(list, HttpStatus.OK);
    	}catch (EmptyResultDataAccessException e){
    		log.error("failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    	}catch(RuntimeException e) {
    		log.error("failed:" + e.getMessage());
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    	}
    }
    
    /**
     * 차량 인식 결과 상세
     * @param userServiceRequest
     * @return
     **/
    @ApiOperation(value = "차량 인식 결과 상세")
    @GetMapping("/view/{id}")
    public ResponseEntity<?> getTestInfo(@CurrentUser UserPrincipal user, @PathVariable Long id) {
    	log.info("/getTestInfo["+user.getUserNo()+"]");
    	//return ResponseEntity.ok(new ApiResponse(true, "조회성공", roadRecogService.getTestInfo(id)));
    	return new ResponseEntity<>(roadRecogService.getTestInfo(id), HttpStatus.OK);
    }
    
    /**
     * 테스트
     * @param userServiceRequest
     * @return
     **/
    @ApiOperation(value = "테스트")
    @PostMapping("/test")
    public ResponseEntity<?> getTest(@CurrentUser UserPrincipal user, @PathVariable Long id) {
    	log.info("/getTest["+user.getUserNo()+"]["+id+"]");
    	//return ResponseEntity.ok(new ApiResponse(true, "조회성공", userEdgeService.getTest(id)));
    	return new ResponseEntity<>(userEdgeService.getTest(id), HttpStatus.OK);
    }
    
    /**
     * 차량 인식 결과 등록
     * @param roadRecogServiceRequest
     * @return
     */
    @ApiOperation(value = "차량 인식 결과 등록")
    @PostMapping("/create")
    //public ResponseEntity<?> addRoadRecog(@CurrentUser UserPrincipal user, @RequestBody UserEdgeDto userEdgeDto, RoadRecogDto roadRecogDto) {
    public ResponseEntity<?> addRoadRecog(@RequestBody String recogJson) {
    	//log.info("/addRoadRecog["+user.getUserNo()+"]");
    	
    	Map<String, Object> map = roadRecogService.addRoadRecog(recogJson);
    	
    	if ("success".equals(map.get("result"))) {
    		//return ResponseEntity.ok(new ApiResponse(true, "등록성공", map));
    		return new ResponseEntity<>(map, HttpStatus.OK);
    	} else {
    		//return new ResponseEntity<>(new ApiResponse(false, "등록실패", map), HttpStatus.BAD_REQUEST);
    		return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
    	}
    }
    
    /**
     * 차량 인식 결과 수정
     * @param roadRecogServiceRequest
     * @return
     */
    @ApiOperation(value = "차량 인식 결과 수정")
    @PostMapping("/edit")
    public ResponseEntity<?> editRoadRecog(@CurrentUser UserPrincipal user, @RequestBody RoadRecogDto roadRecogDto) {
    	//if (userEdgeService.editUserEdge((long)1, userEdgeDto)) {
    	log.info("/editRoadRecog["+user.getUserNo()+"]");
    	if (roadRecogService.editRoadRecog(user.getUserNo(), roadRecogDto)) {
    		//return ResponseEntity.ok(new ApiResponse(true, "수정성공", true));
    		return new ResponseEntity<>(true, HttpStatus.OK);
    	} else {
    		//return new ResponseEntity<>(new ApiResponse(false, "수정실패", false), HttpStatus.BAD_REQUEST);
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    	}
    }
    
}
