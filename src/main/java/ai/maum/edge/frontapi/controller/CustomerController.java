package ai.maum.edge.frontapi.controller;

import ai.maum.edge.frontapi.commons.payload.ApiResponse;
import ai.maum.edge.frontapi.commons.security.CurrentUser;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.model.entity.CustomerQuoteRequest;
import ai.maum.edge.frontapi.model.entity.CustomerServiceRequest;
import ai.maum.edge.frontapi.service.CustomerService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * CustomerController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-21
 */
@RestController
@RequestMapping(value = "/api/customer")
public class CustomerController {

    final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }
    
    /**
     * 견적 문의 코드 조회
     * @param customerServiceRequest
     * @return
     */
    @ApiOperation(value = "견적문의 코드 조회")
    @GetMapping("/edge")
    public ResponseEntity<?> getEdgeList(HttpServletRequest request, HttpServletResponse response) {
    	return ResponseEntity.ok(new ApiResponse(true, "조회성공", customerService.getEdgeList()));
    	
    }
    
    @ApiOperation(value = "엣지 상태 조회")
    @GetMapping("/edge/{code}/status")
    public ResponseEntity<?> getEdgeStatusInfo(@CurrentUser UserPrincipal user, @PathVariable String code) {
    	Map<String, Object> map = customerService.getEdgeStatusInfo(code, user);
    	
    	if ("success".equals(map.get("result"))) {
    		return ResponseEntity.ok(new ApiResponse(true, "조회성공", map));
    	} else {
    		return new ResponseEntity<>(new ApiResponse(false, "조회실패", map), HttpStatus.BAD_REQUEST);    		
    	}
    	
    }

    /**
     * 고객문의 등록
     * @param customerServiceRequest
     * @return
     */
    @ApiOperation(value = "고객문의 등록")
    @PostMapping("/service")
    public ResponseEntity<?> setCustomerServiceRequest(HttpServletRequest request, @RequestBody CustomerServiceRequest customerServiceRequest) {
    	String serverName = request.getServerName();
        if(customerService.setCustomerServiceRequest(customerServiceRequest, serverName)){
            return ResponseEntity.ok(new ApiResponse(true, "등록성공", customerServiceRequest));
        }else{
            return new ResponseEntity<>(new ApiResponse(false, "등록실패", customerServiceRequest), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * 견적문의 등록
     * @param customerQuoteRequest
     * @return
     */
    @ApiOperation(value = "견적문의 등록")
    @PostMapping("/quote")
    public ResponseEntity<?> setCustomerQuoteRequest(HttpServletRequest request, @RequestBody CustomerQuoteRequest customerQuoteRequest) {
    	String serverName = request.getServerName();
        if(customerService.setCustomerQuoteRequest(customerQuoteRequest, serverName)){
            return ResponseEntity.ok(new ApiResponse(true, "등록성공", customerQuoteRequest));
        }else{
            return new ResponseEntity<>(new ApiResponse(false, "등록실패", customerQuoteRequest), HttpStatus.BAD_REQUEST);
        }
    }

}
