package ai.maum.edge.frontapi.controller;

import ai.maum.edge.frontapi.commons.payload.ApiResponse;
import ai.maum.edge.frontapi.commons.security.CurrentUser;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.model.dto.ClientCompanyDto;
import ai.maum.edge.frontapi.model.dto.ClientCompanySearchDto;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ai.maum.edge.frontapi.service.ClientCompanyService;
import io.swagger.annotations.ApiOperation;

/**
 * CustomerController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-21
 */
@RestController
@RequestMapping(value = "/api/clientCompany")
public class ClientCompanyController {

    final ClientCompanyService clientCompanyService;
    
    

    public ClientCompanyController(ClientCompanyService clientCompanyService) {
        this.clientCompanyService = clientCompanyService;
    }
    
    @ApiOperation(value = "회사 리스트 조회")
    @GetMapping
    public ResponseEntity<?> getClientCompanyList(@CurrentUser UserPrincipal user, ClientCompanySearchDto clientCompanySearchDto) {
    	
    	try {
    		Page<ClientCompanyDto.list> list = clientCompanyService.getClientCompanyList(user, clientCompanySearchDto);
    		return ResponseEntity.ok(new ApiResponse(true, "조회성공", list));
    		
    	} catch (Exception e) {
    		return new ResponseEntity<>(new ApiResponse(false, "조회실패", e.getMessage()), HttpStatus.BAD_REQUEST); 
		}
    	
    }
    
    @ApiOperation(value = "회사 등록")
    @PostMapping
    public ResponseEntity<?> addClientCompany(@CurrentUser UserPrincipal user, @RequestBody ClientCompanyDto company) {
    	
    	int result = clientCompanyService.addClientCompany(user.getUserNo(), company);
    	
    	if (result == 1) {
    		return ResponseEntity.ok(new ApiResponse(true, "등록", true));
    	} else if (result == -1) {
    		return ResponseEntity.ok(new ApiResponse(true, "회사명 중복", false));
    	} else {
    		return new ResponseEntity<>(new ApiResponse(false, "등록실패", false), HttpStatus.BAD_REQUEST); 
    	}
    }
    
    @ApiOperation(value = "회사 상세 조회")
    @GetMapping("/{id}")
    public ResponseEntity<?> getClientCompanyInfo(@PathVariable String id) {
    	return ResponseEntity.ok(new ApiResponse(true, "조회성공", clientCompanyService.getClientCompanyInfo(id)));
    }
    
    @ApiOperation(value = "회사 수정")
    @PutMapping("/{id}")
    public ResponseEntity<?> editClientCompany(@CurrentUser UserPrincipal user, @PathVariable String id, @RequestBody ClientCompanyDto dto) {
    	
    	int result = clientCompanyService.editClientCompany(user.getUserNo(), dto);
    	
    	if (result == 1) {
    		return ResponseEntity.ok(new ApiResponse(true, "수정성공", true));    		
    	} else if (result == -1) {
    		return ResponseEntity.ok(new ApiResponse(true, "회사명 중복", false));
    	} else {
    		return new ResponseEntity<>(new ApiResponse(false, "수정실패", false), HttpStatus.BAD_REQUEST);     		
    	}
    	
    }
    
    @ApiOperation(value = "회사 중복 체크")
    @PostMapping("/duplicate")
    public ResponseEntity<?> checkClientCompanyName(@RequestBody ClientCompanyDto dto) {
    	
    	try {
    		boolean isDup = clientCompanyService.checkClientCompanyName(dto);    		
    		return new ResponseEntity<>(isDup, HttpStatus.OK);
    	} catch (Exception e) {
			// TODO: handle exception
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}	
    	
    }
    
    @ApiOperation(value = "회사 삭제")
    @DeleteMapping
    public ResponseEntity<?> delClientCompany(@CurrentUser UserPrincipal user, @RequestBody String[] data) {
    	
    	try {
    		boolean isDel = clientCompanyService.delClientCompany(user.getUserNo(), data);
    		return ResponseEntity.ok(new ApiResponse(true, "중복체크", isDel));    		
    		
    	} catch (RuntimeException e) {
    		return new ResponseEntity<>(new ApiResponse(false, "조회실패", e.getMessage()), HttpStatus.BAD_REQUEST); 
		}
    	
    }
    
}
