package ai.maum.edge.frontapi.controller;

import java.util.Map;

import ai.maum.edge.frontapi.commons.security.CurrentUser;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.model.dto.DeviceSetDto;
import ai.maum.edge.frontapi.model.dto.DeviceSetSearchDto;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ai.maum.edge.frontapi.service.DeviceSetService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * DeviceSetController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-01-27  / 차승호	 / 최초 생성
 * </pre>
 * @since 2021-01-27
 */
@Slf4j
@RestController
//@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/device/set/engineer")
@Secured({"ROLE_ADMIN", "ROLE_ENGINEER"})
public class DeviceSetController {
	
    final DeviceSetService deviceSetService;
    
    public DeviceSetController(DeviceSetService deviceSetService) {
    	this.deviceSetService = deviceSetService;
    }

    /**
     * 엣지 장비 설치 조회
     * @param deviceSetServiceRequest
     * @return
     **/
    @ApiOperation(value = "엣지 장비 설치 조회")
    @PostMapping("/list")
    public ResponseEntity<?> getDeviceSetList(@CurrentUser UserPrincipal user, @RequestBody DeviceSetSearchDto deviceSetSearchDto) {
    //public ResponseEntity<?> getDeviceSetList(@RequestBody DeviceSetSearchDto deviceSetSearchDto) {
    	log.info("/getDeviceSetList["+user.getUserNo()+"]");
    	
    	try {
    		Page<DeviceSetDto> list = deviceSetService.getDeviceSetList(deviceSetSearchDto);
            return new ResponseEntity<>(list, HttpStatus.OK);
    	}catch (EmptyResultDataAccessException e){
    		log.error("failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    	}catch(RuntimeException e) {
    		log.error("failed:" + e.getMessage());
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    	}
    }
    
    /**
     * 엣지 장비 설치 수정
     * @param deviceSetServiceRequest
     * @return
     **/
    @ApiOperation(value = "엣지 장비 설치 수정")
    @PostMapping("/edit")
    public ResponseEntity<?> editDeviceSet(@CurrentUser UserPrincipal user, @RequestBody DeviceSetDto deviceSetDto) {
    	log.info("/editDeviceSet["+user.getUserNo()+"]");
    	
    	Map<String, Object> map =  deviceSetService.editDeviceSet(user.getUserNo(), deviceSetDto);
    	
    	if ("success".equals(map.get("result"))) {
    		//return ResponseEntity.ok(new ApiResponse(true, "등록성공", map));
    		return new ResponseEntity<>(map, HttpStatus.OK);
    	} else {
    		//return new ResponseEntity<>(new ApiResponse(false, "등록실패", map), HttpStatus.BAD_REQUEST);
    		return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
    	}
    }
    
    /**
     * 엣지 장비 연결 체크
     * @param deviceSetServiceRequest
     * @return
     **/
    @ApiOperation(value = "엣지 장비 연결 체크")
    @PostMapping("/duplicate")
    public ResponseEntity<?> checkDeviceAiStatus(@RequestBody DeviceSetDto dto) {
    	
    	try {
    		boolean isDup = deviceSetService.checkDeviceAiStatus(dto);    		
    		return new ResponseEntity<>(isDup, HttpStatus.OK);
    	} catch (Exception e) {
			// TODO: handle exception
    		return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}	
    	
    }
    
    
}
