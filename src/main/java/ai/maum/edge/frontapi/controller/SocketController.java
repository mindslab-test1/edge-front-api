package ai.maum.edge.frontapi.controller;

import ai.maum.edge.frontapi.commons.security.CurrentUser;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.model.dto.DeviceViewDto;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.web.bind.annotation.RestController;

import ai.maum.edge.frontapi.repository.DeviceViewRepository;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class SocketController {
	
	private final DeviceViewRepository deviceViewRepository;
	
	@MessageMapping("/{edgeId}") 
	public void SendToMessage(@DestinationVariable String edgeId, @CurrentUser UserPrincipal user) throws Exception {
		DeviceViewDto dto = DeviceViewDto.builder().edgeId(edgeId).userId(user.getUserNo()).build();
		
		//deviceViewRepository.insertDeviceView(dto);
		
	} 
	

}
