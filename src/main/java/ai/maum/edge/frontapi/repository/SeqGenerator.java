package ai.maum.edge.frontapi.repository;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.Configurable;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.IdentityGenerator;
import org.hibernate.internal.util.config.ConfigurationHelper;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.type.Type;

public class SeqGenerator implements IdentifierGenerator, Configurable {
	
	public static final String SEQ_KEY = "tableName";
	
	private String tableNm;
	
	@Override
	public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
		// TODO Auto-generated method stub
		
		Connection connection = session.connection();
		
		int id=0;
		String code = "";
        try {

            PreparedStatement ps = connection
                    .prepareStatement("SELECT sequence + increment as nextval, prepend_name from sequence_management where table_name= '" + tableNm + "'");

            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                id = rs.getInt("nextval");
                code = rs.getString("prepend_name");
            }
            ps = connection
                    .prepareStatement("update sequence_management set sequence = " + id + " where table_name= '" + tableNm + "'");
            ps.execute();
        } catch (SQLException e) {       
            e.printStackTrace();
        }
        
        String result = code + ((id >= 10) ? id : "0" + id);
       
        return result;
	}

	@Override
	public void configure(Type type, Properties params, ServiceRegistry serviceRegistry) throws MappingException {
		// TODO Auto-generated method stub
		this.tableNm = ConfigurationHelper.getString(SEQ_KEY, params);
	}

}
