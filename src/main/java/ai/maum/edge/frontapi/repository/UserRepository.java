package ai.maum.edge.frontapi.repository;

import ai.maum.edge.frontapi.model.entity.User;
import ai.maum.edge.frontapi.service.UserExInterface;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by kirk@mindslab.ai on 2020-10-22
 */
@Repository
//public interface UserRepository extends JpaRepository<User, Integer> {
public interface UserRepository extends CrudRepository<User, Long>, JpaRepository<User, Long> {
	Optional<User> findById(Long id);
	Optional<User> findByIdAndUseYn(Long id, String useYn);
    //Optional<User> findByNameEmail(String name, String email);
    User findByEmail(String email);
    User findByEmailAndUseYn(String email, String useYn);
    Optional<User> findAllById(Long id);
    //Page<UserExInterface> findAllById(Long id, Pageable pageable);
    Page<UserExInterface> findAllById(String email, Pageable pageable);
    //Page<UserDto> findAll(Pageable pageable);
    //Page<UserExInterface> findAllById(Specification<UserExInterface> spec, Pageable pageable);
    
    /*default Page<User> getUser(EntityManager entityManager, UserSearchDto searchDto, Pageable pageable) {
    	JPAQuery<User> userQuery = new JPAQuery<User>(entityManager)
							.select(user)
							.from(user)
							.where(
									likeSearchType(searchDto.getSearchType(), searchDto.getSearchTxt())
									,isUseYn(searchDto.getUseYn())
									,isRegDate(searchDto.getStartDate(), searchDto.getEndDate())
								  );
		List<ClientCompanyDto> list = userQuery.fetch();
		return new PageImpl(list, pageable, userQuery.fetchCount());
	}
    
    private BooleanExpression likeSearchType(String searchType, String searchTxt) {
		
		
		if ("".equals(searchTxt) || searchTxt  == null) return null;
		if ("".equals(searchType) || searchType == null) return null;
		
		BooleanExpression exp = null;
		
		switch (searchType) {
			case "userEmail":
				exp = user.email.contains(searchTxt);
				break;
			case "userNm":
				exp = user.name.contains(searchTxt);
				break;
			case "userTel":
				exp = user.tel.contains(searchTxt);
				break;
			case "companyId":
				exp = user.clientCompanyId.contains(searchTxt);
				break;
		}
		
		return exp;
	}*/
	
	/*private BooleanExpression isUseYn(String useYn) {
		
		if ("".equals(useYn) || useYn == null) return null;
		return user.useYn.eq(useYn);
	}*/
	
	/*private BooleanExpression isRegDate(String sDate, String eDate) {
		
		boolean isSdate = true;
		boolean isEdate = true;
		BooleanExpression exp = null;
		
		if (("".equals(sDate) || sDate == null)) isSdate = false;
		if (("".equals(eDate) || eDate == null)) isEdate = false;
		
		if (!isSdate && !isEdate) return null;
		
		if (isSdate && isEdate) {
			exp = user.regDt.between(new LocalDateParser(sDate).getStartDate(), new LocalDateParser(eDate).getEndDate());
		} else if (isSdate && !isEdate) {
			exp = user.regDt.gt(new LocalDateParser(sDate).getStartDate());
		} else if (!isSdate && isEdate) {
			exp = user.regDt.lt(new LocalDateParser(eDate).getEndDate());
		}
		
		return exp;
		
	}*/
    
}
