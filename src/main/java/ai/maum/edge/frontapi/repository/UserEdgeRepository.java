package ai.maum.edge.frontapi.repository;

import ai.maum.edge.frontapi.commons.utils.LocalDateParser;
import ai.maum.edge.frontapi.model.dto.UserEdgeDto;
import ai.maum.edge.frontapi.model.dto.UserSearchDto;
import ai.maum.edge.frontapi.model.entity.QClientCompany;
import ai.maum.edge.frontapi.model.entity.QUser;
import ai.maum.edge.frontapi.model.entity.User;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;

import java.util.Optional;

import javax.persistence.EntityManager;

import java.time.LocalDateTime;

/**
 * Created by kirk@mindslab.ai on 2020-10-22
 */
@Repository
//public interface UserRepository extends JpaRepository<User, Integer> {
public interface UserEdgeRepository extends JpaRepository<User, Long> {
    
	QUser user = QUser.user;
	QClientCompany clientCompany = QClientCompany.clientCompany;
	
	Optional<User> findById(Long id);
	
	Optional<User> findByEmail(String email);
	
	
	//조회
    default Page<UserEdgeDto> getUserEdge(EntityManager entityManager, UserSearchDto searchDto, Pageable pageable) {
    	QueryResults<UserEdgeDto> userQuery = new JPAQuery<UserEdgeDto>(entityManager)
				.select(Projections.fields(
											UserEdgeDto.class, 
												user.id, 
												user.email, 
												user.name, 
												user.tel,
												//user.clientCompanyName,
												//clientCompany.clientCompanyName,
												//user.authId,
												//clientCompany.clientCompanyId,
												//clientCompany.clientCompanyName,
												//ExpressionUtils.as(JPAExpressions.select(clientCompany.clientCompanyName).from(clientCompany).where(clientCompany.clientCompanyId.eq(user.clientCompanyId)), "clientCompanyName"),
												//ExpressionUtils.as(JPAExpressions.select(clientCompany.clientCompanyId).from(clientCompany).where(clientCompany.clientCompanyId.eq(user.clientCompanyId)), "clientCompanyId"),
												clientCompany.clientCompanyId,
												clientCompany.clientCompanyName,
												user.authId,
												//user.role,
												user.useYn,
												user.regDt,
												user.modDt
											))
				.from(user)
				.leftJoin(clientCompany).on(clientCompany.clientCompanyId.eq(user.clientCompanyId), clientCompany.useYn.eq("Y"))
				.where(
						likeSearchType(searchDto.getSearchType(), searchDto.getSearchTxt(), searchDto.getClientCompanyId())
						,isAuth(searchDto.getAuth())
						,isUseYn(searchDto.getUseYn())
						,isRegDate(searchDto.getStartDate(), searchDto.getEndDate())
						,equalsClientCompanyId(searchDto.getClientCompanyId())
					  )
				.orderBy(user.regDt.desc(), user.modDt.desc())
				.offset(pageable.getOffset())
				.limit(pageable.getPageSize())
				.fetchResults();
    	/*System.out.println("likeSearch"+likeSearchType(searchDto.getSearchType(), searchDto.getSearchTxt()));
    	int startLimit = searchDto.getPage() == 0 ? 0 : searchDto.getPage() * searchDto.getSize();
    	int endLimit = startLimit + searchDto.getSize();
    	System.out.println("==================startLimit=================");
    	System.out.println(startLimit);
    	System.out.println("==================startLimit=================");
    	System.out.println("==================endLimit=================");
    	System.out.println(endLimit);
    	System.out.println("==================endLimit=================");
    	System.out.println("==================endLimit=================");
    	System.out.println(endLimit);
    	System.out.println("==================endLimit=================");
    	System.out.println("==================userQuery.fetchCount()=================");
    	System.out.println(userQuery.fetchCount());
    	System.out.println("==================userQuery.fetchCount()=================");
    	if(endLimit >= userQuery.fetchCount()) {
    		endLimit = (int) userQuery.fetchCount();
    	}
		final List<UserEdgeDto> list = userQuery.fetch().subList(startLimit, endLimit);*/
		//return new PageImpl(list, pageable, userQuery.fetchCount());
    	return new PageImpl<>(userQuery.getResults(), pageable, userQuery.getTotal());
	}
    
    //삭제
    default long deleteUserEdge(EntityManager entityManager, Long userEdgeId, Long userId) {
		return new JPAQueryFactory(entityManager)
					.update(user)
					.set(user.modDt, LocalDateTime.now())
					.set(user.modId, userId)
					.where(user.id.eq(userEdgeId))
					.execute();
	}
    
    private BooleanExpression equalsClientCompanyId(String clientCompanyId) {
    	if ("".equals(clientCompanyId) || clientCompanyId  == null) return null;
    	
    	return user.clientCompanyId.eq(clientCompanyId);
    }
    
    private BooleanExpression likeSearchType(String searchType, String searchTxt, String clientCompanyId) {
		if ("".equals(searchTxt) || searchTxt  == null) return null;

		BooleanExpression exp = null;
		
		switch (searchType) {
			case "all":
				if (!"".equals(clientCompanyId) && clientCompanyId != null) {
					exp = user.email.contains(searchTxt).or(user.name.contains(searchTxt));
				} else {
					exp = user.email.contains(searchTxt).or(user.name.contains(searchTxt)).or(user.tel.contains(searchTxt)).or(clientCompany.clientCompanyName.contains(searchTxt));					
				}
				break;
			case "userEmail":
				exp = user.email.contains(searchTxt);
				break;
			case "userNm":
				exp = user.name.contains(searchTxt);
				break;
			case "userTel":
				exp = user.tel.contains(searchTxt);
				break;
			case "companyName":
				//exp = user.clientCompany.contains(searchTxt);
				exp = clientCompany.clientCompanyName.contains(searchTxt);
				break;
		}
		
		System.out.println(exp);
		return exp;
	}
	
	private BooleanExpression isUseYn(String useYn) {
		if ("".equals(useYn) || useYn == null) return null;
		return user.useYn.eq(useYn);
	}
	
	private BooleanExpression isAuth(Long auth) {
		if ("".equals(auth) || auth == null) return null;
		return user.authId.eq(auth);
	}
	
	private BooleanExpression isRegDate(String sDate, String eDate) {
		
		boolean isSdate = true;
		boolean isEdate = true;
		BooleanExpression exp = null;
		
		if (("".equals(sDate) || sDate == null)) isSdate = false;
		if (("".equals(eDate) || eDate == null)) isEdate = false;
		
		if (!isSdate && !isEdate) return null;
		
		if (isSdate && isEdate) {
			exp = user.regDt.between(new LocalDateParser(sDate).getStartDate(), new LocalDateParser(eDate).getEndDate());
		} else if (isSdate && !isEdate) {
			exp = user.regDt.gt(new LocalDateParser(sDate).getStartDate());
		} else if (!isSdate && isEdate) {
			exp = user.regDt.lt(new LocalDateParser(eDate).getEndDate());
		}
		
		return exp;
		
	}
    
}
