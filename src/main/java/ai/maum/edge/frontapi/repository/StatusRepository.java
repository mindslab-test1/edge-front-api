package ai.maum.edge.frontapi.repository;

import ai.maum.edge.frontapi.model.entity.Status;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * ServiceRequestRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-22  / 이성주	 / 최초 생성
 * </pre>
 * @since 2020-12-21
 */
@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {

}
