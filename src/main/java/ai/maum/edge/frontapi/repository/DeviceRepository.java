package ai.maum.edge.frontapi.repository;

import ai.maum.edge.frontapi.model.entity.Device;
import ai.maum.edge.frontapi.model.entity.QDevice;
import ai.maum.edge.frontapi.model.entity.QUser;
import ai.maum.edge.frontapi.model.entity.User;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;

/**
 * ServiceRequestRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-21
 */
@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {
	
	QDevice device = QDevice.device;
	QUser user = QUser.user;
	
	List<Device> findByEdgeTypeAndUserIdAndUseYn(String edgeType, Long userId, String useYn);
	
	List<Device> findByEdgeTypeAndUseYn(String edgeType, String useYn);
	
	Optional<Device> findByEdgeId(String edgeId);
	
	Optional<Device> findByDeviceId(Long deviceId);
	
	Device findByEdgeIdIgnoreCaseAndDeviceIdNot(String edgeId, Long deviceId);
	
	default List<String> findByEdgePaymentList(EntityManager entityManager, User user) {
		return new JPAQueryFactory(entityManager)
				.select(device.edgeType)
				.from(device)
				.where(device.user.eq(user), device.useYn.eq("Y"))
				.groupBy(device.edgeType)
				.fetch();
	}
	
	
	default List<Device> findByDeviceIds(EntityManager entityManager, List<Long> deviceIds) {
		return new JPAQueryFactory(entityManager)
					.select(device)
					.from(device)
					.where(device.deviceId.in(deviceIds), device.useYn.eq("Y"))
					.fetch();
	}
	
	default List<Device> findByEdgeTypeAndClientCompanyIdAndUseYn(EntityManager entityManager, String edgeType, String clientCompanyId, String useYn){
		return new JPAQueryFactory(entityManager)
				.select(Projections.fields(
						Device.class,
						device.deviceId
						, device.edgeId
						, device.edgeType
						, user.clientCompanyId
						))
				.from(device)
				.leftJoin(user).on(device.userId.eq(user.id))
				.where(
						device.edgeType.eq(edgeType)
						, device.useYn.eq("Y")
						, user.clientCompanyId.eq(clientCompanyId)
						)
				.fetch();
	}
}
