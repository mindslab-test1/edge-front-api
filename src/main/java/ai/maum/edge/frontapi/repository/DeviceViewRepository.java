package ai.maum.edge.frontapi.repository;

import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import ai.maum.edge.frontapi.model.dto.DeviceViewDto;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class DeviceViewRepository {
	
	private static final String DEVICE_VIEW = "DEVICE_VIEW";
	
	private final RedisTemplate<String, Object> redisTemplate;
	
	private HashOperations<String, String, DeviceViewDto> opsHashDevice;
	
	@PostConstruct
	private void init() {
		opsHashDevice = redisTemplate.opsForHash();
	}
	
	public List<DeviceViewDto> findAllDeviceView() {
		return opsHashDevice.values(DEVICE_VIEW);
	}
	
	public DeviceViewDto findByDeviceView(String edgeId) {
		return opsHashDevice.get(DEVICE_VIEW, edgeId);
	}
	
	public List<DeviceViewDto> findByDeviceView(Long userId) {
		List<DeviceViewDto> opDto = opsHashDevice.values(DEVICE_VIEW).stream().filter(dto -> dto.getUserId().equals(userId)).collect(Collectors.toList());
		
		return opDto;
	}
	
	public void insertDeviceView(DeviceViewDto deviceView) {
		opsHashDevice.put(DEVICE_VIEW, deviceView.getEdgeId(), deviceView);
	}
	
	public void removeDeviceView(Long userId) {
		List<DeviceViewDto> dtoList = this.findByDeviceView(userId);
		
		if (dtoList != null) {
			for (DeviceViewDto dto : dtoList) {
				this.removeDeviceView(dto.getEdgeId());				
			}
		}
	}
	
	public void removeDeviceView(String edgeId) {
		opsHashDevice.delete(DEVICE_VIEW, edgeId);
	}
}
