package ai.maum.edge.frontapi.repository;

import ai.maum.edge.frontapi.model.entity.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by kirk@mindslab.ai on 2020-10-05
 */
@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
	Role findByAuthId(Long id);
    Role findByAuthName(String name);
}
