package ai.maum.edge.frontapi.model.entity;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_ENGINEER,
    ROLE_COMP_ADMIN
}
