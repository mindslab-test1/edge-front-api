package ai.maum.edge.frontapi.model.dto;


import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DeviceSetSearchDto {
	
	private String searchType;
	
	private String searchTxt;
	
	private String edgeAiStatus;
	
	private String startDate;
	
	private String endDate;
	
	private String setState;
	
	private int page;
	
	private int size;
	
}
