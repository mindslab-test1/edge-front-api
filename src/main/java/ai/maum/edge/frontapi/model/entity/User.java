package ai.maum.edge.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;
import java.time.LocalDateTime;

@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"clientCompany", "userCompanyHistories","devices"})
@Table(name = "user", indexes = {@Index(columnList = "email")})
public class User {

	@Id
	@Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    
    //@Column(insertable = false, updatable = false)
    //private int mappingSeq;

	@Column(length = 24, insertable = false, updatable = false) 
	private String clientCompanyId; // client_company_id(PK) 회사 아이디(PK) 아이디 VARCHAR(24) NOT NULL
	
	@Column(insertable = false, updatable = false)
	private Long authId;
	
	@Column(nullable = false)
    private String name;
    
    @Column(nullable = false)
    private String email;
    
    @Column(length = 13) 
    private String tel; // tel 회원 연락처 전화번호 VARCHAR(13) NOT NULL

    @Column(length = 1)
    private String useYn;
    
    @Column 
    private Long regId; // reg_id 등록자 아이디 VARCHAR(24) NOT NULL
	
    @Column(name="reg_dt", nullable = false, updatable = false)
	@CreationTimestamp //private LocalDateTime regDt; // reg_dt 등록일시 일시 DATETIME	NOT NULL 
	private LocalDateTime regDt; // reg_dt 등록일시 일시 DATETIME NOT NULL
    
	@Column(length = 24) 
    private Long modId; // mod_id 수정자 아이디 VARCHAR(24) NULL
    
	@Column(name="mod_dt")
    @CreationTimestamp
	//private LocalDateTime modDt; // mod_dt 수정일시 일시 DATETIME NULL
	private LocalDateTime modDt; // mod_dt 수정일시 일시 DATETIME NULL
    
    @ManyToOne
    @JoinColumn(name="authId")
    private Role role;

    private String password;

    public User (Long id) {
        this.id = id;
    }
    
    @ManyToOne
    @JoinColumn(name = "clientCompanyId")
    private ClientCompany clientCompany; // 고객사
    /*@JoinColumns(
    		{
	    		@JoinColumn(updatable=false,insertable=false, name="clientCompanyId", referencedColumnName="clientCompanyId"),
	            @JoinColumn(updatable=false,insertable=false, name="clientCompanyName", referencedColumnName="clientCompanyName"),
    		}
    )*/
    
    @OneToMany(fetch = FetchType.LAZY,  mappedBy = "user")
    private Set<Device> devices = new LinkedHashSet<>(); // 디바이스
    
    //@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "user", orphanRemoval=true)
    //private Set<UserCompanyHistory> userCompanyHistories = new LinkedHashSet<>(); // 유저 고객사 매핑 이력
    
    @PrePersist
    public void prePersist() {
        this.regDt = Optional.ofNullable(this.regDt).orElse(LocalDateTime.now());
        this.useYn = Optional.ofNullable(this.useYn).orElse("Y");
    }
    
    @PreRemove
    private void preRemove() {
        for (Device device : devices) {
            device.setUser(null);
        }
    }
	/*
	 * @Id
	 * 
	 * @GeneratedValue(strategy = GenerationType.IDENTITY) private Long id;
	 * 
	 * @Column(length = 24) private String clientCompanyId; // client_company_id(PK)
	 * 회사 아이디(PK) 아이디 VARCHAR(24) NOT NULL
	 * 
	 * @Column(nullable = false, length = 24) private String userName; // user_name
	 * 회원명 이름 VARCHAR(24) NOT NULL
	 * 
	 * @Column(nullable = false, length = 24) private String userEmail; //
	 * user_email 회원 이메일 아이디 VARCHAR(24) NOT NULL
	 * 
	 * @Column(nullable = false, length = 13) private String userTel; // user_tel 회원
	 * 연락처 전화번호 VARCHAR(13) NOT NULL
	 * 
	 * @Column(nullable = false, length = 1) private String useYn; // use_yn 사용여부 여부
	 * CHAR(1) NOT NULL
	 * 
	 * private String apiId;
	 * 
	 * private String apiKey;
	 * 
	 * @Column(nullable = false, length = 24) private String regId; // reg_id 등록자
	 * 아이디 VARCHAR(24) NOT NULL
	 * 
	 * @Column(name="reg_dt", nullable = false, updatable = false)
	 * 
	 * @CreationTimestamp //private LocalDateTime regDt; // reg_dt 등록일시 일시 DATETIME
	 * NOT NULL private Date regDt; // reg_dt 등록일시 일시 DATETIME NOT NULL
	 * 
	 * @Column(length = 24) private String modId; // mod_id 수정자 아이디 VARCHAR(24) NULL
	 * 
	 * @Temporal(TemporalType.TIMESTAMP)
	 * 
	 * @Column //private LocalDateTime modDt; // mod_dt 수정일시 일시 DATETIME NULL
	 * private Date modDt; // mod_dt 수정일시 일시 DATETIME NULL
	 * 
	 * @ManyToOne
	 * 
	 * @JoinColumn(name="role_id", nullable = false, updatable = false) private Role
	 * role;
	 * 
	 * private String password;
	 * 
	 * public User (Long id) { this.id = id; }
	 */
}
