package ai.maum.edge.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

import org.hibernate.annotations.CreationTimestamp;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

/**
 * 엣지 로드
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-19  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"device","policyHistories","roadRecogs"})
@Table(
        indexes = {
        }
)
public class Road implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long edgeNodeSeq; // edge_node_seq(PK)	노드 순번(PK)	순번	BIGINT	NOT NULL
    
    @Column(nullable = false, length = 12, insertable = false, updatable = false)
    private Long deviceId;
    
    @Column
    private String description; // desc	설치장소 (api)	설명	VARCHAR(255)	NULL

    @Column(length = 20)
    private String connect; // connect	connect	N/A	VARCHAR(20)	NULL

    @Column
    private String url; // url	검지영상 PATH	경로	VARCHAR(255)	NULL

    @Column(length = 20)
    private String cameraDomain; // camera_domain	camera_domain	N/A	VARCHAR(20)	NULL
    
    @Column(length = 50)
    private String cameraDir; // camera_dir	camera_dir	N/A	VARCHAR(50)	NULL
    
    @Column
    private int cameraNo; // camera_no	camera_no	N/A	BIGINT	NULL

    @Column
    private String roi; // roi	roi 영역좌표	N/A	VARCHAR(255)	NULL

    @Column(length = 10)
    private String camDir; // cam_dir	cam_dir	N/A	VARCHAR(10)	NULL

    @Column
    private String oneWay; // one_way	one_way	N/A	BOOLEAN	NULL

    @Column(length = 10)
    private String vehSide; // veh_side	검지구분 (전/후면)	구분	VARCHAR(10)	NULL

    @Column
    private int startLane; // start_lane	시작차선 검지정보	N/A	SMALLINT	NULL

    @Column
    private int rangeLane; // lanes	범위차선 검지정보	N/A	SMALLINT	NULL
    
    @Column(nullable = false, columnDefinition = "VARCHAR(1) default 'Y'")
    private String useYn; // use_yn	사용여부	여부	CHAR(1)	NOT NULL

    @Column(nullable = false)
    private Long regId; // reg_id	등록자	아이디	VARCHAR(24)	NOT NULL

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt; // reg_dt	등록일시	일시	DATETIME	NOT NULL

    @Column
    private Long lastModId; // last_mod_id	최종 수정자	아이디	VARCHAR(24)	NULL

    @Column
    private LocalDateTime lastModDt; // last_mod_dt	최종 수정일시	일시	DATETIME	NULL

    @Column
    private LocalDateTime updated; // updated	정책 적용 일자	N/A	DATETIME	NULL

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="deviceId")
    private Device device; // 디바이스

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "road")
    private Set<PolicyHistory> policyHistories = new LinkedHashSet<>(); // 정책 이력

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "road")
    private Set<RoadRecog> roadRecogs = new LinkedHashSet<>(); // 엣지 로드 인식결과
    
    @PrePersist
    public void prePersist() {
    	this.regDt = Optional.ofNullable(this.regDt).orElse(LocalDateTime.now());
    	this.useYn = Optional.ofNullable(this.useYn).orElse("Y");
    }
    
    @PreRemove
    private void preRemove() {
        for (RoadRecog r : roadRecogs) {
            r.setRoad(null);
        }
        
        for (PolicyHistory p : policyHistories) {
        	p.setRoad(null);
        }
    }

}
