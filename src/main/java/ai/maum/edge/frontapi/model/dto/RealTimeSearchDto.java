package ai.maum.edge.frontapi.model.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class RealTimeSearchDto implements Serializable {
	
	private String clientCompanyId;
	
	private String addr;
	
	private String addrDetail;
	
	private String cameraDir;
	
	private boolean allAddr;
	
	private Long deviceId;
	
}
