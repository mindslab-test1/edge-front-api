package ai.maum.edge.frontapi.model.dto;

import lombok.Getter;

@Getter
public class LoginDto {

    private String id;
    private String password;

}
