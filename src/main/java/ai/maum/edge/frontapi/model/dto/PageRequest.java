package ai.maum.edge.frontapi.model.dto;

import org.springframework.data.domain.Sort;

import lombok.Getter;
import lombok.Setter;

@Getter
public final class PageRequest {
	
	private int page;
	
	private int size;
	
	private Sort.Direction direction;

	public void setPage(int page) {
		this.page = page <= 0 ? 1 : page;
	}

	public void setSize(int size) {
		int DEFAULT_SIZE = 10;
		int MAX_SIZE = 50;
		
		this.size = size > MAX_SIZE ? DEFAULT_SIZE : size;
	}

	public void setDirection(Sort.Direction direction) {
		this.direction = direction;
	}
	
	
	public org.springframework.data.domain.PageRequest of(String order) {
		
		if (this.size <= 0) {
			return null;
		}
		
		return org.springframework.data.domain.PageRequest.of(page - 1, size, Sort.by("reg_dt").descending());
	}
}
