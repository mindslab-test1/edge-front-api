package ai.maum.edge.frontapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
public class TokenVO {
    private String email;
    private String name;
    private String phone;

    private String apiId;
    private String apiKey;

    private String access_token;
    private String access_expire_time;

    private String refresh_token;                    // AccessToken을 재발급(재로그인) 받기위한 토큰
    private String refresh_expire_time;

    private int db_diff_time;
    
    @Setter
    private String uuid;
}
