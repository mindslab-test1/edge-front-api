package ai.maum.edge.frontapi.model.entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;

import java.time.LocalDateTime;

/**
 * 회원 - 고객사 매핑 이력
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-19  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"road"})
@Table(
        indexes = {
        		@Index(name = "IX_road_recog", columnList = "recogSeq,passTime,addr,addrDetail,cameraDir,modName,clientCompanyName,carNumber,editTime,edgeId,direction,lane,carType,confLevel")
        }
)
public class RoadRecog {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long recogSeq; // recog_seq(PK)	인식결과 순번(PK)	순번	BIGINT	NOT NULL
    
    @Column(updatable=false, insertable=false)
    private Long edgeNodeSeq; // edge_node_seq(FK)	노드 순번(FK)	순번	BIGINT	NOT NULL
    
    //@Column(nullable = false)
    //private Long deviceId; // edge_id(FK)	엣지 아이디(FK)	엣지아이디	VARCHAR(255)	NOT NULL
    
    //@Temporal(TemporalType.TIMESTAMP)
    @Column
    private LocalDateTime passTime; // pass_time	pass_time	일시	DATETIME	NULL
    
    //@Temporal(TemporalType.TIMESTAMP)
    @Column
    private LocalDateTime editTime; // edit_time	edit_time	일시	DATETIME	NULL
    
    @Column(length = 40)
    private String carNumber; // car_number	car_number	차량번호	VARCHAR(40)	NULL
    
    @Column(length = 20)
    private String carType; // car_type	car_type	차량유형	VARCHAR(20)	NULL
    
    @Column
    private String carImgPath; // car_img_path	car_img_path	경로	VARCHAR(255)	NULL
    
    @Column
    private String plateImgPath; // plate_img_path	plate_img_path	경로	VARCHAR(255)	NULL
    
    @Column(length = 10)
    private String direction; // direction	전후면 구분	구분	VARCHAR(10)	NULL
    
    @Column
    private float confLevel; // conf_level	conf_level	N/A	FLOAT	NULL
    
    @Column
    private int lane; // lane	lane	N/A	SMALLINT	NULL
    
    @Column
    private float speed; // speed	speed	N/A	FLOAT	NULL
    
    @Column
    private Long deviceId; // device_id(PK)	엣지 순번	엣지 순번	BIGINT	NULL
    
    @Column
    private String edgeId; // edge_id(PK)	엣지 아이디		엣지아이디	VARCHAR(255)	NULL
    
    @Column
    private String addr; // addr_code	설치장소 코드	코드	VARCHAR(255)	NULL

    @Column
    private String addrDetail; // addr_detail	설치장소 상세	상세	VARCHAR(255)	NULL
    
    @Column(length = 50)
    private String cameraDir; // camera_dir	camera_dir	N/A	VARCHAR(50)	NULL
    
    @Column(length = 24)
    private String clientCompanyId; // client_company_id(PK)   회사 아이디(PK)   아이디   VARCHAR(24)   NOT NULL
    
    @Column(length = 80)
    private String clientCompanyName; // client_company_name(PK)   회사명(PK)   이름   VARCHAR(100)   NOT NULL
    
    @Column
    private int cameraNo; // camera_no	camera_no	N/A	BIGINT	NULL
    
    @Column(length = 24) 
    private Long userId; // user_id 아이디 아이디 VARCHAR(24) NULL
    
    @Column(length = 24) 
    private Long modId; // mod_id 수정자 아이디 VARCHAR(24) NULL
    
    @Column(length = 50)
    private String modName; // mod_nm 수정자 이름 VARCHAR(24) NULL
    
	//@Column(name="mod_dt")
    //@CreationTimestamp
	//private LocalDateTime modDt; // mod_dt 수정일시 일시 DATETIME NULL
    
    @Lob
    @Column(nullable = false)
    private String requestJson; // request_json	요청원문	JSON	LONGTEXT	NOT NULL
    
    @ManyToOne
    @JoinColumns(
            {
                    @JoinColumn(name="edgeNodeSeq", referencedColumnName="edgeNodeSeq"),
                    //@JoinColumn(updatable=false,insertable=false, name="deviceId", referencedColumnName="deviceId"),
            }
    )
    private Road road; // 엣지 로드
}