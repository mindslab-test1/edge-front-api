package ai.maum.edge.frontapi.model.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

import ai.maum.edge.frontapi.model.dto.MenuDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@SqlResultSetMapping(
			name = "topMenuMapping",
			classes = @ConstructorResult(
						targetClass = MenuDto.class,
						columns = {
								@ColumnResult(name="id", type = Long.class),
								@ColumnResult(name="menuCode", type = String.class),
								@ColumnResult(name="menuName", type = String.class),
								@ColumnResult(name="url", type = String.class)
						}
					)
		)
@Entity
@Setter
@Getter
@NoArgsConstructor
public class TopMenu {
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;				// id(PK) 메뉴 코드(PK) 아이디 VARCHAR(24) NOT NULL
	
	@Column(length = 24, nullable = false)
	private String menuCode;	 // menu(PK) 메뉴 코드(PK) 아이디 VARCHAR(24) NOT NULL
	
	@Column(length = 100, nullable = false)
	private String menuName;	// menuName 메뉴 이름 NOT NULL
	
	@Column
	private int ord;			// ord 정렬 순서
	
	@Column(length = 1, nullable = false)
	private String useYn;		// useYn	사용 여부 NOT NULL
	
	@Column(length = 1, nullable = false)
	private String showYn;		// showYn	구매여부와 관계 엾이 노출 여부 NOT NULL
	
	@Transient
	private String url;
	
	@Column 
    private Long regId; // reg_id 등록자 아이디 VARCHAR(24) NOT NULL
	
    @Column(name="reg_dt")
	@CreationTimestamp 
	private LocalDateTime regDt; // reg_dt 등록일시 일시 DATETIME NOT NULL
    
	@Column(length = 24) 
    private Long modId; // mod_id 수정자 아이디 VARCHAR(24) NULL
    
	@Column(name="mod_dt")
    @CreationTimestamp
	private LocalDateTime modDt; // mod_dt 수정일시 일시 DATETIME NULL
	
}
