package ai.maum.edge.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Setter
@Getter
@Table(name = "ROLE")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long authId;
    
    @Column(nullable = false, length = 24)
    private String authName;

//    @OneToMany(mappedBy = "role")
//    @JsonIgnore
//    private List<User> users;
}
