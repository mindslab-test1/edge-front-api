package ai.maum.edge.frontapi.model.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MenuDto {
	
	private Long id;
	
	private String menuCode;
	
	private String menuName;
	
	private String url;
	
	public MenuDto (MenuExInterface menu) {
		this.id = menu.getId();
		this.menuCode = menu.getMenuCode();
		this.menuName = menu.getMenuName();
		this.url = menu.getUrl();
	}
	
	public MenuDto (Long id, String menuCode, String menuName, String url) {
		this.id = id;
		this.menuCode = menuCode;
		this.menuName = menuName;
		this.url = url;
	}
	
}
