package ai.maum.edge.frontapi.model.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Setter
@Getter
@NoArgsConstructor
@ToString
public class RoadRecogDto implements Serializable {
	
	private String edgeId;
	private String addr;
	private String addrDetail;
	private String cameraDir;
	private int cameraNo;
	private String useYn;
	
	private String clientCompanyId;
	private String clientCompanyName;
	
	private Long recogSeq;
	private Long edgeNodeSeq;
	private LocalDateTime passTime;
	private LocalDateTime editTime;
	private String carNumber;
	private String carType;
	private String carImgPath;
	private String plateImgPath;
	private String direction;
	private float confLevel;
	private int lane;
	private float speed;
	private Long modId;
	private String modName;
	
	@Getter
    @Setter
    @NoArgsConstructor
    @ToString
    @EqualsAndHashCode(callSuper = true)
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class RoadRecogEdit extends RoadRecogDto {
		private String carNumber;
		private String carType;
    }
	
}
