package ai.maum.edge.frontapi.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.persistence.*;

/**
 * 엣지 장비 설치 정보
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-01-06  / 차승호	 / 최초 생성
 * </pre>
 * @since 2020-12-19
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(exclude = {"device"})
@Table(
        indexes = {
                @Index(name = "IX_device_set", columnList = "deviceId"),
                @Index(name = "IX_device_set2", columnList = "setYn,setUserId"),
        }
)
public class DeviceSet {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long edgeSetSeq; // edge_set_seq(PK)	장비 이력 순번(PK)	순번	BIGINT	NOT NULL

    @Column(nullable = false, insertable = false, updatable = false)
    private Long deviceId; // edge_id(FK)	엣지 아이디(FK)	엣지아이디	VARCHAR(255)	NOT NULL
    
    @Column(nullable = false, columnDefinition = "VARCHAR(1) default 'N'")
    private String setYn;	// set_yn	설치여부	여부	CHAR(1)	NOT NULL

    @Column
    private Long setUserId; // set_user_id	설치자	아이디	VARCHAR(24)	NOT NULL
    
    //@CreationTimestamp
    @Column
    private LocalDateTime setDt; // set_dt	설치완료일시	일시	DATETIME	NULL
    
    @Column(nullable = false)
    private Long regId; // reg_id	등록자	아이디	VARCHAR(24)	NOT NULL

    @Column(nullable = false, updatable = false)
    private LocalDateTime regDt; // reg_dt	등록일시	일시	DATETIME	NOT NULL

    @Column
    private Long modId; // mod_id	수정자	아이디	VARCHAR(24)	NULL

    @Column
    private LocalDateTime modDt; // mod_dt	수정일시	일시	DATETIME	NULL
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="deviceId")
    private Device device; // 엣지 장비 정보
    
    @PrePersist
    public void prePersist() {
    	this.regDt = Optional.ofNullable(this.regDt).orElse(LocalDateTime.now());
    }

}
