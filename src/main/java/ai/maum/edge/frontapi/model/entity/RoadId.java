package ai.maum.edge.frontapi.model.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RoadId implements Serializable {
	
	private Long edgeNodeSeq;
	
	private String edgeId;
	
}
