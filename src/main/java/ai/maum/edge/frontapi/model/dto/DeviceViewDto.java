package ai.maum.edge.frontapi.model.dto;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class DeviceViewDto implements Serializable {
	
	private String edgeId;
	
	private Long userId;
	
	@Builder
	public DeviceViewDto(String edgeId, Long userId) {
		this.edgeId = edgeId;
		this.userId = userId;
	}
}
