package ai.maum.edge.frontapi.service;

import ai.maum.edge.frontapi.commons.enums.Api;
import ai.maum.edge.frontapi.commons.utils.ModelMapperService;
import ai.maum.edge.frontapi.commons.utils.RestUtil;
import ai.maum.edge.frontapi.config.CacheKey;
import ai.maum.edge.frontapi.model.entity.User;
import ai.maum.edge.frontapi.repository.ClientCompanyRepository;
import ai.maum.edge.frontapi.repository.RoleRepository;
import ai.maum.edge.frontapi.repository.UserEdgeRepository;
import ai.maum.edge.frontapi.model.dto.PageRequest;
import ai.maum.edge.frontapi.model.dto.UserEdgeDto;
import ai.maum.edge.frontapi.model.dto.UserSearchDto;
import ai.maum.edge.frontapi.model.entity.ClientCompany;
import ai.maum.edge.frontapi.model.entity.Role;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Service
public class UserEdgeService {
	@Autowired
    RoleRepository roleRepository;
	
	@Autowired
    ClientCompanyRepository clientCompanyRepository;
	
    Logger logger = LoggerFactory.getLogger(UserEdgeService.class);
    
    final UserEdgeRepository userEdgeRepository;
    final EntityManager entityManager;
    final ModelMapperService modelMapperService;
    final RestUtil restUtil;

    public UserEdgeService(UserEdgeRepository userEdgeRepository, EntityManager entityManager, ModelMapperService modelMapperService, RestUtil restUtil) {
        this.userEdgeRepository = userEdgeRepository;
        this.entityManager = entityManager;
        this.modelMapperService = modelMapperService;
        this.restUtil = restUtil;
    }
    
    public boolean addUserEdge(Long userId, UserEdgeDto userEdgeDto) {
    	User user = modelMapperService.map(userEdgeDto, User.class);
    	user.setRegId(userId);
    	//user.setDelYn("N");
    	user.setPassword("1234");
    	//user.setAuthId(userEdgeDto.getAuthId());
    	Role userRole = roleRepository.findByAuthId(userEdgeDto.getAuthId());
    	user.setRole(userRole);
		return Optional.ofNullable(userEdgeRepository.save(user)).isPresent();
    }
    
    
    /*@Transactional
    public Page<UserDto.UserDtoEx> list(Long id, Pageable pageable) {

//        Page<Project> list = projectRepository.findAllByUserIdOrderByIdDesc(userNo, pageable);
//        return list.map(project -> modelMapperService.map(project, ProjectDto.class));
    	
        Page<UserExInterface> list2 = userRepository.findAllById(id, pageable);
        
        //List<User> list2 = userRepository.getUserList(entityManager, params);
        Page<UserDto.UserDtoEx> p2 = list2.map(user -> modelMapperService.map(user, UserDto.UserDtoEx.class));
        return p2;
    }*/
    
    @Transactional
    public Page<UserEdgeDto> getUserEdgeList(UserSearchDto userSearchDto) {
    	PageRequest page = new PageRequest();
    	
    	page.setPage(userSearchDto.getPage());
    	page.setSize(userSearchDto.getSize());
    	page.setDirection(Sort.Direction.DESC);
    	
		return userEdgeRepository.getUserEdge(entityManager, userSearchDto, page.of("regDt"));
        
    }
    
    //테스트
    public UserEdgeDto getTest(Long id) {
    	Optional<User> user = userEdgeRepository.findById((long)1);
    	if (user.isPresent()) {
    		UserEdgeDto dto = modelMapperService.map(user.get(), UserEdgeDto.class);    		
    		return dto;
    	} else {
    		return null;
    	}
    }
    
    //상세
    public UserEdgeDto getUserEdgeInfo(Long id) {
    	Optional<User> user = userEdgeRepository.findById(id);
    	if (user.isPresent()) {
    		UserEdgeDto dto = modelMapperService.map(user.get(), UserEdgeDto.class);    		
    		return dto;
    	} else {
    		return null;
    	}
    }
    
    //수정
    public boolean editUserEdge(Long userId, UserEdgeDto userEdgeDto) {
    	Optional<User> c = userEdgeRepository.findById(userEdgeDto.getId());
    	//User user = modelMapperService.map(userEdgeDto, User.class);
    	if (c.isPresent()) {
    		//User editUser = c.get();
    		User editUser = modelMapperService.map(c.get(), User.class);
    		editUser.setName(userEdgeDto.getName());
    		editUser.setEmail(userEdgeDto.getEmail());
    		editUser.setTel(userEdgeDto.getTel());
    		editUser.setUseYn(userEdgeDto.getUseYn());
    		editUser.setModDt(LocalDateTime.now());
    		editUser.setModId(userId);
    		editUser.setClientCompanyId(userEdgeDto.getClientCompanyId());
    		
    		editUser.setAuthId(userEdgeDto.getAuthId());
        	Role userRole = roleRepository.findByAuthId(userEdgeDto.getAuthId());
        	editUser.setRole(userRole);
    		ClientCompany clientCompany = clientCompanyRepository.findByClientCompanyId(userEdgeDto.getClientCompanyId());
    		editUser.setClientCompany(clientCompany);
    		return Optional.ofNullable(userEdgeRepository.save(editUser)).isPresent();
    		
    	} else {
    		return false;
    	}
    	
    }
    
    //삭제
    @Transactional
    public boolean deleteUserEdge(Long userId, long[] arr) {
    	for (Long userEdgeId : arr) {
    		//userEdgeRepository.deleteUserEdge(entityManager, userEdgeId, userId);
    		Optional<User> c = userEdgeRepository.findById(userEdgeId);
    		if(c.isPresent()) {
    			userEdgeRepository.deleteById(userEdgeId);
    		}else {
    			return false;
    		}
    		
    	}
    	return true;
    	
    }
    
    //사용자 API 검색
    public Map<String, Object> getUserSearch(Map<String, String> codeMap, Long userId) {
    	Map<String, Object> resultMap = new HashMap<String, Object>();
    	try {
    		
    		//api 전송
    		Map<String, Object> params = new HashMap<String, Object>();
    		params.put("code", "MINDS_SSO");
    		params.put("type", codeMap.get("searchType"));
    		params.put("sch", codeMap.get("searchTxt"));
    		params.put("page", codeMap.get("page"));
    		params.put("size", codeMap.get("size"));
    		params.put("sort", "name,asc");
    		resultMap = restUtil.get(Api.SEARCH_USER, params);
    		if(resultMap != null) {
    			resultMap.put("result", "success");
    		}else {
    			resultMap.put("result", "fail");
    			resultMap.put("msg", resultMap.get("error"));
    		}
    	} catch(Exception e) {
    		resultMap.put("result", "fail");
    		resultMap.put("msg", e.getMessage());
    	}
    	
    	return resultMap;
    	
    }
    
    //사용자 중복 조회
    public boolean checkUserEmail(UserEdgeDto userEdgeDto) {
    	if(userEdgeRepository.findByEmail(userEdgeDto.getEmail()).isPresent()) {
    		return true;
    	}else {
    		return false;
    	}
    	
    }
    
    @Caching(evict = {
    		@CacheEvict(value = CacheKey.loadUserById, key="#id"),
    		@CacheEvict(value = CacheKey.getMenuByUserId, key="#id")
    })
    public void removeCache(Long id) {
    	logger.info("userId : " + id + " 캐시 삭제 완료");
    }
}