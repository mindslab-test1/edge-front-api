package ai.maum.edge.frontapi.service;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

import ai.maum.edge.frontapi.commons.enums.Api;
import ai.maum.edge.frontapi.repository.DeviceHistoryRepository;
import ai.maum.edge.frontapi.repository.PolicyHistoryRepository;
import ai.maum.edge.frontapi.repository.StatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ai.maum.edge.frontapi.model.entity.Device;
import ai.maum.edge.frontapi.model.entity.DeviceHistory;
import ai.maum.edge.frontapi.model.entity.PolicyHistory;
import ai.maum.edge.frontapi.model.entity.Road;
import ai.maum.edge.frontapi.model.entity.Status;

/**
 * DeviceService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일        / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-22  / 이성주   / 최초 생성
 * </pre>
 * @since 2020-01-22
 */
@Service
public class HistoryService {

	Logger logger = LoggerFactory.getLogger(HistoryService.class);
    final DeviceHistoryRepository historyRepository;
    final StatusRepository statusRepository;
    final PolicyHistoryRepository policyRepository;

    public HistoryService(DeviceHistoryRepository historyRepository, StatusRepository statusRepository, PolicyHistoryRepository policyRepository) {
        this.historyRepository = historyRepository;
        this.statusRepository = statusRepository;
        this.policyRepository = policyRepository;
    }
    
    public boolean addDeviceHistory(Long deviceId, Api api, Map<String, Object> param, Map<String, Object> response) {
    	
    	DeviceHistory history = new DeviceHistory();
    	
    	history.setAction(api.name());
    	history.setRequest(param.toString());
    	history.setResponse(response.toString());
    	
    	if (deviceId == null) {
    		history.setDevice(null);
    	} else {
    		Device device = new Device();
    		device.setDeviceId(deviceId);
    		
    		history.setDevice(device);    		
    	}
    	
    	
    	if(Optional.ofNullable(historyRepository.save(history)).isPresent()) {
    		return true;
    	} else {
    		return false;    		
    	}
    	
    }
    
    public boolean addStatus(Long deviceId, Map<String, Object> response) {
    	Status status = new Status();
    	
    	if (deviceId == null) {
    		status.setDevice(null);
    	} else {
    		Device device = new Device();
    		device.setDeviceId(deviceId);
    		status.setDevice(device);
    	}
    	
    	
    	status.setResponse(response.toString());
    	
    	if (Optional.ofNullable(statusRepository.save(status)).isPresent()) {
    		return true;
    	} else {
    		return false;
    	}
    	
    }
    
    public boolean addPolicyHistory(Long edgeNodeSeq, float version, LocalDateTime updated, Api api, Map<String, Object> request, Map<String, Object> response) {
    	PolicyHistory policy = new PolicyHistory();
    	
    	if (edgeNodeSeq == 0) {
    		policy.setRoad(null);
    	} else {
    		Road road = new Road();
    		road.setEdgeNodeSeq(edgeNodeSeq);
    		
    		policy.setRoad(road);
    	}
    	
    	policy.setAction(api.name());
    	policy.setRequest(request.toString());
    	policy.setResponse(response.toString());
    	policy.setVersion(version);
    	policy.setUpdated(updated);
    	
    	if (Optional.ofNullable(policyRepository.save(policy)).isPresent()) {
    		return true;
    	} else {
    		return false;
    	}
    }
        
}
