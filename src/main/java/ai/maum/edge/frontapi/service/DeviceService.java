package ai.maum.edge.frontapi.service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import ai.maum.edge.frontapi.commons.enums.Api;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.commons.utils.ModelMapperService;
import ai.maum.edge.frontapi.commons.utils.RestUtil;
import ai.maum.edge.frontapi.model.dto.DeviceDto;
import ai.maum.edge.frontapi.model.entity.Device;
import ai.maum.edge.frontapi.model.entity.RoleName;
import ai.maum.edge.frontapi.model.entity.User;
import ai.maum.edge.frontapi.repository.DeviceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

/**
 * DeviceService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일        / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-22  / 이성주   / 최초 생성
 * </pre>
 * @since 2020-01-22
 */
@Service
@RequiredArgsConstructor
public class DeviceService {

	Logger logger = LoggerFactory.getLogger(DeviceService.class);
    private final DeviceRepository deviceRepository;
    private final HistoryService historyService;
    private final DeviceSetService deviceSetService;
    private final EntityManager entityManager;
    private final ModelMapperService modelMapperService;
    private final RestUtil restUtil;
    
    
    public DeviceDto.info getEdgeInfo(Long deviceId) {
    	DeviceDto.info info = new DeviceDto.info();
    	
    	Optional<Device> deviceOp = deviceRepository.findByDeviceId(deviceId);
    	
    	if (deviceOp.isEmpty()) return info;
    	
    	Device device = deviceOp.get();
    	
    	info = modelMapperService.mapAmbiguityIgnored(device, DeviceDto.info.class);
    	
    	List<String> paramList = Arrays.asList(device.getEdgeId());
    	
    	Map<String, Object> params = new HashMap<String, Object>();
		params.put("edge_id_list", paramList);
		
		Map<String, Object> result = restUtil.post(Api.GET_EDGE_STATUS, params);
		
		String apiResult = result.get("status") == null ? "" : (String) result.get("status");
		
		if (result != null && "ok".equals(apiResult.toLowerCase())) {
			List<Object> moduleStatus = (List<Object>) result.get("edges_module_status");
			
			if (moduleStatus != null && moduleStatus.size() > 1) {
				List<String> explanList = (List<String>) moduleStatus.get(0);
				List<String> edgeInfoList = ((Map<String, List<String>>)moduleStatus.get(1)).get(device.getEdgeId());
				
				info.setExplanList(explanList);
				info.setEdgeInfoList(edgeInfoList);
				
				/*Map<String, Object> infoMap = new HashMap<String, Object>();
				
				for (int i = 0; i < explanList.size(); i++) {
					infoMap.put(toCamelCase(explanList.get(i)), edgeInfoList.get(i));
				}
				
				info = modelMapperService.mapToClass(infoMap, DeviceDto.info.class);*/
				
			}
			
		}
		
		historyService.addStatus(deviceId, result);
    	
		return info;
		
    }
    
    
    @Transactional(rollbackOn = {RuntimeException.class, Exception.class})
    public boolean addEdgeDevice(Device device, Long regId) {
    	
    	//회사 아이디 넣지 않는다.
    	device.setClientCompanyId(null);
    	device.setClientCompany(null);
    	
    	device.setReleaseDate(new Date());
    	device.setRegId(regId);
    	
    	Optional<Device> isDevice = deviceRepository.findByEdgeId(device.getEdgeId());
    	
    	if (isDevice.isPresent()) {
    		device.setDeviceId(isDevice.get().getDeviceId());
    		device.setUseYn("Y");
    	}
    	
    	if (Optional.ofNullable(deviceRepository.save(device)).isPresent()) {
    		
    		if (deviceSetService.addDeviceHistory(device, regId)) {
    			return true;
    		} else {
    			throw new RuntimeException("deviceSetService 등록 중 오류 발생");
    		}
    		
    	} else {
    		throw new RuntimeException("device 등록 중 오류 발생");
    	}
    	
    }
    
    public String restartEdge(Long deviceId) {
    	
    	Optional<Device> device = deviceRepository.findByDeviceId(deviceId);
    	
    	if (device.isEmpty()) return "fail";
    	
    	Map<String, Object> param = new HashMap<String, Object>();
    	
    	param.put("edge_id", device.get().getEdgeId());
    	
    	Map<String, Object> result = restUtil.post(Api.EDGE_RESTART, param);
    	
    	if (result != null) {
    		String apiResult = result.get("status") == null ? "" : (String) result.get("status");
        	
        	
        	historyService.addDeviceHistory(deviceId, Api.EDGE_RESTART, param, result);
        	
        	if ("ok".equals(apiResult.toLowerCase())) {
        		return "succ";
        	} else if ("fail".equals(apiResult.toLowerCase()) && String.valueOf(result.get("error_cause")).indexOf("") > -1) {
        		return "notconnect";
        	}
    	}
    	
    	return "fail";
    }
    
    public boolean editDevice(Device editDevice, UserPrincipal user) {
    	
    	Optional<Device> device = deviceRepository.findByEdgeId(editDevice.getEdgeId());
    	
    	if (device.isPresent()) {
    		Device setDevice = device.get();
    		setDevice.setAddr(editDevice.getAddr());
    		setDevice.setAddrDetail(editDevice.getAddrDetail());
    		
    		if (user.getAuthorities().stream().anyMatch(item -> item.getAuthority().equals(RoleName.ROLE_ADMIN.name()))) {
    			if (editDevice.getUserId() != null && editDevice.getUserId() != 0) {
    				User userDto = new User();
    				userDto.setId(editDevice.getUserId());
        			setDevice.setUser(userDto);			
        		}
        		
        		/*if (editDevice.getClientCompanyId() != null && !"".equals(editDevice.getClientCompany())) {
        			ClientCompany company = new ClientCompany();
        			company.setClientCompanyId(editDevice.getClientCompanyId());
        			setDevice.setClientCompany(company);    			
        		}*/
        	}
    		
    		setDevice.setLastModId(user.getUserNo());
    		setDevice.setLastModDt(LocalDateTime.now());
    		
    		
    		return Optional.ofNullable(deviceRepository.save(setDevice)).isPresent();
    		
    	}
    	
    	return false;
    }
    
    public boolean checkEdgeId(DeviceDto dto) {
    	
    	if (dto.getEdgeId() == null || "".equals(dto.getEdgeId())) return false;
    	
    	if (Optional.ofNullable(deviceRepository.findByEdgeIdIgnoreCaseAndDeviceIdNot(dto.getEdgeId(), (dto.getDeviceId() != null ? dto.getDeviceId() : (long) 0))).isPresent()) {
    		return false;
    	} else {
    		return true;
    	}
    }
    
    public static String toCamelCase(String s) {
        String result = "";
        String[] tokens = s.split(" "); // or whatever the divider is
        for (int i = 0, L = tokens.length; i<L; i++) {
            String token = tokens[i];
            if (i==0) result = token.toLowerCase();
            else
                result += token.substring(0, 1).toUpperCase() +
                    token.substring(1, token.length()).toLowerCase();
        }
        return result;
    }
}
