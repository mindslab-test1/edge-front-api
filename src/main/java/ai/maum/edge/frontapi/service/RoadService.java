package ai.maum.edge.frontapi.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import ai.maum.edge.frontapi.commons.enums.Api;
import ai.maum.edge.frontapi.commons.enums.Code;
import ai.maum.edge.frontapi.commons.payload.ApiResponse;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.commons.utils.ModelMapperService;
import ai.maum.edge.frontapi.commons.utils.RestUtil;
import ai.maum.edge.frontapi.model.entity.Device;
import ai.maum.edge.frontapi.model.entity.DeviceSet;
import ai.maum.edge.frontapi.model.entity.Road;
import ai.maum.edge.frontapi.model.entity.RoadRecog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ai.maum.edge.frontapi.model.dto.RoadDto;
import ai.maum.edge.frontapi.model.dto.DeviceDto;
import ai.maum.edge.frontapi.model.dto.DeviceSearchDto;
import ai.maum.edge.frontapi.model.dto.DeviceViewDto;
import ai.maum.edge.frontapi.model.dto.PageRequest;
import ai.maum.edge.frontapi.model.dto.RealTimeSearchDto;
import ai.maum.edge.frontapi.repository.DeviceRepository;
import ai.maum.edge.frontapi.repository.DeviceViewRepository;
import ai.maum.edge.frontapi.repository.RecentlyRoadRepository;
import ai.maum.edge.frontapi.repository.RoadRecogRepository;
import ai.maum.edge.frontapi.repository.RoadRepository;
import ai.maum.edge.frontapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;

/**
 * DeviceService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일        / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-01-22  / 이성주   / 최초 생성
 * </pre>
 * @since 2020-01-22
 */
@Service
@RequiredArgsConstructor
public class RoadService {

	Logger logger = LoggerFactory.getLogger(RoadService.class);
	private final UserRepository userRepository;
	private final RoadRecogRepository roadRecogRepository;
	private final RecentlyRoadRepository recentlyRoadRepository;
	private final RoadRepository roadRepository;
	private final DeviceRepository deviceRepository;
	private final DeviceService deviceSerivce;
	private final HistoryService historyService;
	private final EntityManager entityManager;
	private final ModelMapperService modelMapperService;
	private final RestUtil restUtil;
	private final DeviceViewRepository deviceViewRepository;

    
    public Page<DeviceDto.list> getEdgeRoadList(DeviceSearchDto deviceSearchDto, UserPrincipal user) {
    	
    	PageRequest page = new PageRequest();
    	
    	page.setPage(deviceSearchDto.getPage());
    	page.setSize(deviceSearchDto.getSize());
    	page.setDirection(Sort.Direction.DESC);
    	
    	String compId = userRepository.findAllById(user.getUserNo()).get().getClientCompanyId();
    	
    	Page<DeviceDto.list> data = roadRepository.getEdgeRoadList(entityManager, deviceSearchDto, user, page.of("regDt"), compId);
    	
    	if (data.hasContent()) {
    		List<String> list = data.stream().map(dto -> dto.getEdgeId()).collect(Collectors.toList());
    		
    		Map<String, Object> params = new HashMap<String, Object>();
    		params.put("edge_id_list", list);
    		
    		Map<String, Object> result = restUtil.post(Api.GET_EDGE_STATUS, params);
    		
    		String apiResult = result.get("status") == null ? "" : (String) result.get("status");
    		
    		if (result != null && "ok".equals(apiResult.toLowerCase())) {
    			Map<String, Object> statusMap = (Map<String, Object>) result.get("edges_device_status");
    			List<Object> moduleStatus = (List<Object>) result.get("edges_module_status");
    			List<String> explanList = (moduleStatus != null && moduleStatus.size() > 0) ? (List<String>) moduleStatus.get(0) : Collections.emptyList();
    			Map<String, List<String>> moduleMap = (moduleStatus != null && moduleStatus.size() > 1) ? (Map<String, List<String>>) moduleStatus.get(1) : new HashMap<String, List<String>>();
    			
    			data.getContent().stream().forEach(device -> {
    				device.setApiData(statusMap.get(device.getEdgeId()));
    				device.setExplanList(explanList);
    				device.setEdgeInfoList((moduleMap.get(device.getEdgeId()) != null) ? moduleMap.get(device.getEdgeId()) : Collections.emptyList());
    				
    			});
    			
    			if (deviceSearchDto.getEdgeAiStatus() != null && !"".equals(deviceSearchDto.getEdgeAiStatus())) {
    				List<DeviceDto.list> filterList = data.getContent().stream().filter(d -> deviceSearchDto.getEdgeAiStatus().equals(d.getEdgeAiStatus())).collect(Collectors.toList());
    				
    		        int fromIndex = deviceSearchDto.getSize() * (deviceSearchDto.getPage() - 1);
    		        int toIndex = deviceSearchDto.getSize() * deviceSearchDto.getPage();
    		        if (toIndex > filterList.size()) {
    		            toIndex = filterList.size();
    		        }
    		        if (fromIndex > toIndex) {
    		            fromIndex = toIndex;
    		        }
    		        
    		        return new PageImpl<DeviceDto.list>(filterList.subList(fromIndex, toIndex), page.of("regDt"), filterList.size()); 
    				
    			}
    			
    			//Page<DeviceDto> pageList = new PageImpl<DeviceDto>(deviceList, page.of("regDt"), data.getTotalPages());
    			
    		}
    		
    		return data;
    		
    	} else {
    		return data;
    	}
    	
    }
    
    @Transactional(rollbackOn = {RuntimeException.class, Exception.class})
    public int addEdgeRoad(DeviceDto.RoadList dto, Long userId) {
    	
    	if (!deviceSerivce.checkEdgeId(dto)) {
    		return -1;
    	}
    	
		Device device = modelMapperService.map(dto, Device.class);
    	device.setEdgeType(Code.EDGE_ROAD.getCode());
    	device.setRoad(new LinkedHashSet<>());
    	
    	List<RoadDto> roadList = dto.getRoadList();
    	
    	if (roadList != null && roadList.size() > 0) {
    		
    		if (deviceSerivce.addEdgeDevice(device, userId)) {
    			for (RoadDto roadDto : roadList) {
        			Road road = modelMapperService.map(roadDto, Road.class);
        			
        			road.setCameraNo(1);
        			road.setDevice(device);
        			road.setRegId(userId);
        			
        			if (!Optional.ofNullable(roadRepository.save(road)).isPresent()) {
        				throw new RuntimeException("road 등록 중 오류");
        			}
        		}
    			
    			return 1;
    			
    		} else {
    			return -2;
    		}
    		
    	} else {
    		return -2;
    	}
    	
    }
    
    public DeviceDto.RoadList detailEdgeRoad(Long deviceId) {
    	
    	Optional<Device> device = deviceRepository.findByDeviceId(deviceId);
    	
    	DeviceDto.RoadList roadList = modelMapperService.mapAmbiguityIgnored(device.get(), DeviceDto.RoadList.class);
    	
    	roadList.setUserId(device.get().getUserId());
    	roadList.setClientCompanyId(device.get().getUser() == null ? null : device.get().getUser().getClientCompanyId());
    	roadList.setClientCompanyName(device.get().getUser() != null && device.get().getUser().getClientCompany() != null ? device.get().getUser().getClientCompany().getClientCompanyName() : null);
    	roadList.setName(device.get().getUser() == null ? null : device.get().getUser().getName());
    	
    	roadList.setRoadList(modelMapperService.mapAll(device.get().getRoad(), RoadDto.class));
    	
    	
    	return roadList;
    	
    }
    
    @Transactional(rollbackOn = {RuntimeException.class, Exception.class})
    public boolean editEdgeRoad(DeviceDto.RoadList dto, UserPrincipal user) {
    	
		Device editDevice = new Device();
    	
    	editDevice.setDeviceId(dto.getDeviceId());
    	editDevice.setEdgeId(dto.getEdgeId());
    	editDevice.setAddr(dto.getAddr());
    	editDevice.setAddrDetail(dto.getAddrDetail());
    	editDevice.setClientCompanyId(dto.getClientCompanyId());
    	editDevice.setUserId(dto.getUserId());
    	
    	List<RoadDto> roadList = dto.getRoadList();
    	
    	if (roadList != null && roadList.size() > 0) {
    		
    		if (deviceSerivce.editDevice(editDevice, user)) {
    			
    			for (RoadDto roadDto : roadList) {
    				Optional<Road> opRoad = roadRepository.findByEdgeNodeSeq(roadDto.getEdgeNodeSeq());
    				
    				if (opRoad.isPresent()) {
    					Road road = opRoad.get();
    					
    					road.setCameraNo(1);
    					road.setCameraDir(roadDto.getCameraDir());
    					road.setLastModId(user.getUserNo());
    					road.setLastModDt(LocalDateTime.now());
    					
    					if(!Optional.ofNullable(roadRepository.save(road)).isPresent()) {
    						throw new RuntimeException("road 수정 중 오류 발생");
    					};
    				} else {
    					throw new RuntimeException("존재하지 않는 road 수정 시도");
    				}
    				
            		
    			}
    			
        	} else {
        		throw new RuntimeException("device 수정 중 오류 발생");
        	}
    		
    		return true;
    	} else {
    		return false;
    	}
    	
    }
    
    @Transactional(rollbackOn = {RuntimeException.class, Exception.class})
    public boolean delEdgeRoad(List<Long> deviceIds, Long userId) {
    	
    	List<Device> list = deviceRepository.findByDeviceIds(entityManager, deviceIds);
    	
		for (Device dto : list) {
			
			deviceRepository.delete(dto);
			
			/*
			 * if (!deviceSerivce.delDevice(dto.getDeviceId(), userId)) { throw new
			 * RuntimeException("device 삭제 오류"); }
			 * 
			 * if (roadRepository.delRoadAll(entityManager, dto.getDeviceId(), userId) <= 0)
			 * { throw new RuntimeException("road 삭제 오류"); }
			 */
    	}
		
		
		for (Device dto : list) {
			
			boolean isSetup = false;
			
			for (DeviceSet set : dto.getDeviceSet()) {
				if ("Y".equals(set.getSetYn())) {
					isSetup = true;
					break;
				}
			}
			
			if (!isSetup) continue;
			
			Map<String, Object> param = new HashMap<String, Object>();
    		param.put("edge_id", dto.getEdgeId());
    		
    		Map<String, Object> result = restUtil.post(Api.EDGE_UNREGISTER, param);
    		
    		if (result != null) {
    			historyService.addDeviceHistory(null, Api.EDGE_UNREGISTER, param, result);
    			String apiResult = result.get("status") == null ? "" : (String) result.get("status");
    			
    			if (!"ok".equals(apiResult.toLowerCase())) {
    				throw new RuntimeException("api 삭제 실패");
            	} 
    		} else {
    			throw new RuntimeException("api 결과값 없음");
    		}
    		
		}
		
    	return false;
    }
    
    public List<DeviceDto.list> refreshEdgeRoadInfo(List<DeviceDto.list> dtoList) {
    	
    		List<String> list = dtoList.stream().map(dto -> dto.getEdgeId()).collect(Collectors.toList());
    		
    		Map<String, Object> params = new HashMap<String, Object>();
    		params.put("edge_id_list", list);
    		
    		Map<String, Object> result = restUtil.post(Api.GET_EDGE_STATUS, params);
    		
    		String apiResult = result.get("status") == null ? "" : (String) result.get("status");
    		
    		if (result != null && "ok".equals(apiResult.toLowerCase())) {
    			Map<String, Object> statusMap = (Map<String, Object>) result.get("edges_device_status");
    			List<Object> moduleStatus = (List<Object>) result.get("edges_module_status");
    			List<String> explanList = (moduleStatus != null && moduleStatus.size() > 0) ? (List<String>) moduleStatus.get(0) : Collections.emptyList();
    			Map<String, List<String>> moduleMap = (moduleStatus != null && moduleStatus.size() > 1) ? (Map<String, List<String>>) moduleStatus.get(1) : new HashMap<String, List<String>>();
    			
    			dtoList.stream().forEach(device -> {
    				device.setApiData(statusMap.get(device.getEdgeId()));
    				device.setExplanList(explanList);
    				device.setEdgeInfoList((moduleMap.get(device.getEdgeId()) != null) ? moduleMap.get(device.getEdgeId()) : Collections.emptyList());
    				
    			});
    			
    		}
    		
    	return dtoList;
    }
    
    public RoadDto.Policy edgeRoadPolicyDetail(Long edgeNodeSeq) {
    	RoadDto.Policy policy = null;
    	Optional<Road> road = roadRepository.findByEdgeNodeSeq(edgeNodeSeq);
    	
    	if (road.isPresent()) {
    		policy = modelMapperService.mapAmbiguityIgnored(road.get(), RoadDto.Policy.class);    		
    	}
    	
    	return policy;
    }
    
    @Transactional(rollbackOn = {RuntimeException.class, Exception.class})
    public boolean edgeRoadPolicyEdit(Long userId, RoadDto.Policy dto) {
    	Optional<Road> opRoad = roadRepository.findByEdgeNodeSeq(dto.getEdgeNodeSeq());
    	
    	if (opRoad.isPresent()) {
    		Road road = opRoad.get();
    		road.setRoi(dto.getRoi());
            road.setStartLane(dto.getStartLane());
            road.setRangeLane(dto.getRangeLane());
            road.setCamDir(dto.getCamDir());
            road.setVehSide(dto.getVehSide());
            road.setConnect(dto.getConnect());
            road.setUrl(dto.getUrl());
            road.setOneWay(dto.getOneWay());
            road.setCameraDomain(dto.getCameraDomain());
            road.setUpdated(LocalDateTime.now());
            road.setLastModId(userId);
            road.setLastModDt(LocalDateTime.now());
            
            if (Optional.ofNullable(roadRepository.save(road)).isPresent()) {
            	
            	//api 전송
            	Map<String, Object> result = this.sendUpdatePolicy(road);
            	
            	if (result != null) {
            		String apiResult = result.get("status") == null ? "" : (String) result.get("status");
        			
        			if ("ok".equals(apiResult.toLowerCase())) {
        				Optional<Device> device = deviceRepository.findByDeviceId(road.getDeviceId());
        				recentlyRoadRepository.deleteRecentlyRoad(device.get().getEdgeId());
        				return true;
                	} else {
                		throw new RuntimeException(Api.EDGE_UPDATE_POLICY.name() + " 업데이트 실패");
                	}
        			
            	} else {
            		throw new RuntimeException(Api.EDGE_UPDATE_POLICY.name() + " 전송 실패");
            	}
            	
            	
            } else {
            	throw new RuntimeException("road 정책 수정 중 오류 발생");
            }
    	} else {
    		throw new RuntimeException("존재하지 않는 road 정보");
    	}
    }
    
    public ApiResponse edgeRoadRealTime(UserPrincipal user, RealTimeSearchDto dto) {
    	
    	DeviceDto.RoadDetail detail = null;
    	String compId = userRepository.findAllById(user.getUserNo()).get().getClientCompanyId();
    	List<Device> deviceList = roadRepository.getEdgeRoadInfo(entityManager, user, dto, compId);
    	
    	if (deviceList != null && deviceList.size() > 0) {
    		
    		Device device = null;
    		
    		for (Device d : deviceList) {
    			DeviceViewDto viewDto = deviceViewRepository.findByDeviceView(d.getEdgeId());
    			
    			if (viewDto == null || viewDto.getUserId().equals(user.getUserNo())) {
    				device = d;
    				break;
    			}
    		}
    		
    		if (device != null) {
    			
    			if (device.getRoad().size() > 0) {
        			Road road = device.getRoad().stream().findFirst().get();
        			
        			Map<String, Object> params = new HashMap<String, Object>();
            		
            		params.put("edge_id", device.getEdgeId());
            		
            		Map<String, Object> result = restUtil.post(Api.EDGE_STREAM_URL, params);
            		
            		if (result != null) {
            			String apiResult = result.get("status") == null ? "" : (String) result.get("status");
            			
            			if ("ok".equals(apiResult.toLowerCase())) {
            				road.setUrl(String.valueOf(result.get("rtsp_url")));
            				detail = modelMapperService.mapAmbiguityIgnored(device, DeviceDto.RoadDetail.class);
                			detail.setPolicy(modelMapperService.mapAmbiguityIgnored(road, RoadDto.Policy.class));
                			detail.setClientCompanyId(device.getUser() == null ? null : device.getUser().getClientCompanyId());
                			
                			DeviceViewDto view = DeviceViewDto.builder()
                								 .edgeId(device.getEdgeId())
                								 .userId(user.getUserNo())
                								 .build();
                			
                			deviceViewRepository.removeDeviceView(user.getUserNo());
                			deviceViewRepository.insertDeviceView(view);
            				
                    	} else {
                    		return new ApiResponse(false, "fail", null);
                    	}
            		}
        			
        		} else {
        			return new ApiResponse(false, "fail", null);
        		}
    		} else {
    			return new ApiResponse(false, "alreadyUse", null);
    		}
    		
    		
    	} 
    	
    	return new ApiResponse(true, "success", detail);
    }
    
    public Map<String, Object> edgeRoadRealAddrList(UserPrincipal user,RealTimeSearchDto dto) {
    	
    	Map<String, Object> result = new HashMap<String, Object>();
    	String compId = userRepository.findAllById(user.getUserNo()).get().getClientCompanyId();
    	if (dto.isAllAddr()) {
    		
    		String[] addrArr = {"addr","addrDetail","cameraDir"};
    		for (String addr : addrArr) {
    			result.put(addr, roadRepository.getEdgeAddrList(entityManager, user, dto, addr, compId));
    		}
     		
    	} else {
    		String groupPath = "";
    		
    		if ("".equals(dto.getAddr()) || dto.getAddr() == null) {
        		groupPath = "addr";
    		} else if ("".equals(dto.getAddrDetail()) || dto.getAddrDetail() == null) {
    			groupPath = "addrDetail";
    		} else if ("".equals(dto.getCameraDir()) || dto.getCameraDir() == null) {
    			groupPath = "cameraDir";
    		}
    		
    		result.put(groupPath, roadRepository.getEdgeAddrList(entityManager, user, dto, groupPath, compId));
    	}
    	
    	
    	return result;
    }
    
    
    public Map<String, Object> sendUpdatePolicy(Road road) {
    	//api 전송
    	Map<String, Object> params = new HashMap<String, Object>();
    	Map<String, Object> dataMap = new HashMap<String, Object>();
    	List<Map<String, Object>> recogCriteriaList = new ArrayList<Map<String,Object>>();
    	
    	Device device = road.getDevice();
    	
    	String desc = device.getAddr() + " " + device.getAddrDetail() + " " + road.getCameraDir() + " 방향";
    	
    	List<Float> roiList = Arrays.asList((road.getRoi().split(","))).stream().map(roi -> Float.parseFloat(roi)).collect(Collectors.toList());
    	
    	params.put("edge_id", road.getDevice().getEdgeId());
    	
    	dataMap.put("desc", desc);
    	dataMap.put("connect", road.getConnect());
    	dataMap.put("url", road.getUrl());
    	dataMap.put("camera_domain", road.getCameraDomain());
    	dataMap.put("roi", roiList);
    	dataMap.put("cam_dir", road.getCamDir());
    	dataMap.put("one_way", road.getOneWay());
    	dataMap.put("veh_side", road.getVehSide());
    	dataMap.put("start_lane", road.getStartLane());
    	dataMap.put("lanes", road.getRangeLane());
    	dataMap.put("updated", road.getUpdated().now().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
    	
    	recogCriteriaList.add(dataMap);
    	
    	//params.put("recog_criteria", recogCriteriaList);
    	params.put("recog_criteria", dataMap);
    	
    	Map<String, Object> result = restUtil.post(Api.EDGE_UPDATE_POLICY, params);
    	
		historyService.addPolicyHistory(road.getEdgeNodeSeq(), (float) 1.0, road.getUpdated(), Api.EDGE_UPDATE_POLICY, params, result);
    	
    	return result;
    }
    
    public List<Map<String, Object>> recentlyEdgeRoadInfo(String edgeId){
    	
    	Optional<Device> device = deviceRepository.findByEdgeId(edgeId);
        
        Device devices = modelMapperService.map(device.get(), Device.class);
        
        Road road = roadRepository.findByDeviceId(devices.getDeviceId());
        
        List<Map<String, Object>> recentlyRecog = recentlyRoadRepository.findByEdgeId(edgeId);
        List<Map<String, Object>> filterList = new ArrayList<Map<String, Object>>();
        List<RoadRecog> roadRecog1;
        if("both".equals(road.getVehSide())) {
        	roadRecog1 = roadRecogRepository.findByEdgeIdAndLane(edgeId, road.getStartLane());
        }else {
        	roadRecog1 = roadRecogRepository.findByEdgeIdAndLaneAndDirection(edgeId, road.getStartLane(), road.getVehSide());
        }
        
        
        if(roadRecog1.size() == 0) {
        	Map<String, Object> recog1 = new HashMap<String, Object>();
        	recog1.put("edge_id", edgeId);
        	recog1.put("direction", road.getVehSide());
        	recog1.put("lane", road.getStartLane());
        	filterList.add(recog1);
        }else {
        	List<Map<String, Object>> filterList1 = recentlyRecog.stream().filter(data -> Integer.parseInt(String.valueOf(data.get("lane"))) == road.getStartLane()).collect(Collectors.toList());
        	filterList.add(filterList1.get(0));
        }
        
        if(road.getStartLane() != road.getStartLane()+road.getRangeLane()-1) {
        	List<RoadRecog> roadRecog2;
        	if("both".equals(road.getVehSide())) {
        		roadRecog2 = roadRecogRepository.findByEdgeIdAndLane(edgeId, road.getStartLane()+road.getRangeLane()-1);
            }else {
            	roadRecog2 = roadRecogRepository.findByEdgeIdAndLaneAndDirection(edgeId, road.getStartLane()+road.getRangeLane()-1, road.getVehSide());
            }
            
            if(roadRecog2.size() == 0) {
            	Map<String, Object> recog1 = new HashMap<String, Object>();
            	recog1.put("edge_id", edgeId);
            	recog1.put("direction", road.getVehSide());
            	recog1.put("lane", road.getStartLane()+road.getRangeLane()-1);
            	filterList.add(recog1);
            }else {
            	List<Map<String, Object>> filterList2 = recentlyRecog.stream().filter(data -> Integer.parseInt(String.valueOf(data.get("lane"))) == road.getStartLane()+road.getRangeLane()-1).collect(Collectors.toList());
            	filterList.add(filterList2.get(0));
            }
        }
        
        return filterList;
    }
}
