package ai.maum.edge.frontapi.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import ai.maum.edge.frontapi.commons.enums.HomeInfo;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.commons.utils.ModelMapperService;
import ai.maum.edge.frontapi.commons.utils.RestUtil;
import ai.maum.edge.frontapi.model.dto.HomeInfoDto;
import ai.maum.edge.frontapi.repository.DeviceRepository;
import ai.maum.edge.frontapi.repository.QuoteRequestRepository;
import ai.maum.edge.frontapi.repository.ServiceRequestRepository;
import ai.maum.edge.frontapi.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ai.maum.edge.frontapi.commons.enums.Api;
import ai.maum.edge.frontapi.commons.enums.Code;
import ai.maum.edge.frontapi.commons.enums.CodeGroup;
import ai.maum.edge.frontapi.commons.enums.Email;
import ai.maum.edge.frontapi.model.entity.CustomerQuoteRequest;
import ai.maum.edge.frontapi.model.entity.CustomerServiceRequest;
import ai.maum.edge.frontapi.model.entity.Device;
import ai.maum.edge.frontapi.model.entity.RoleName;
import ai.maum.edge.frontapi.model.entity.User;

/**
 * CustomerService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-21
 */
@Service
public class CustomerService {

	Logger logger = LoggerFactory.getLogger(CustomerService.class);
	final ModelMapperService modelMapperService;
	final EntityManager entityManager;
	final UserRepository userRepository;
    final ServiceRequestRepository serviceRequestRepository;
    final QuoteRequestRepository quoteRequestRepository;
    final DeviceRepository deviceRepository;
    final RestUtil restUtil;

    public CustomerService(ModelMapperService modelMapperService, EntityManager entityManager, UserRepository userRepository, ServiceRequestRepository serviceRequestRepository, QuoteRequestRepository quoteRequestRepository, DeviceRepository deviceRepository, RestUtil restUtil) {
    	this.modelMapperService = modelMapperService;
    	this.entityManager = entityManager;
    	this.userRepository = userRepository;
        this.serviceRequestRepository = serviceRequestRepository;
        this.quoteRequestRepository = quoteRequestRepository;
        this.deviceRepository = deviceRepository;
        this.restUtil = restUtil;
    }
    
    /**
     * 엣지 리스트 정보 조회
     * @param
     * @return
     */
    public List<HomeInfoDto> getEdgeList() {
		/*
		 * Optional<List<HomeInfoDto>> edgeList =
		 * homeInfoRepository.getEdgeInfoList(entityManager);
		 * 
		 * if (edgeList.isEmpty()) return null;
		 */
    	
    	return HomeInfo.getDtoList();
    	
    }
    
    public Map<String, Object> getEdgeStatusInfo(String code, UserPrincipal user) {
		
    	Map<String, Object> resultMap = new HashMap<String, Object>();
    	
    	
    	try {
    		
    		boolean isAdmin = user.getAuthorities().stream().anyMatch(role -> role.getAuthority().equals(RoleName.ROLE_ADMIN.name()));
            boolean isEngineer = user.getAuthorities().stream().anyMatch(role -> role.getAuthority().equals(RoleName.ROLE_ENGINEER.name()));
            boolean isCompAdmin = user.getAuthorities().stream().anyMatch(role -> role.getAuthority().equals(RoleName.ROLE_COMP_ADMIN.name()));
    		String useYn = "Y";
    		Long usNo = user.getUserNo();
        	String compId = "";
        	if(usNo != null && usNo != 0) {
        		Optional<User> opUser = userRepository.findAllById(user.getUserNo());
        		if(opUser.isPresent()) {
        			User authUser = modelMapperService.map(opUser.get(), User.class);
        			compId = authUser.getClientCompanyId() == null || authUser.getClientCompanyId() == "" ? "" : authUser.getClientCompanyId();
        		}
        	}
    		
    		List<Device> deviceList = null;
    		if(isCompAdmin) {
    			deviceList = deviceRepository.findByEdgeTypeAndClientCompanyIdAndUseYn(entityManager, code, compId, useYn);
    		}else {
    			deviceList = (isAdmin || isEngineer) ? deviceRepository.findByEdgeTypeAndUseYn(code, useYn) : deviceRepository.findByEdgeTypeAndUserIdAndUseYn(code, user.getUserNo(), useYn);
    		}
    		//List<Device> deviceList = (isAdmin || isEngineer) ? deviceRepository.findByEdgeTypeAndUseYn(code, useYn) : deviceRepository.findByEdgeTypeAndUserIdAndUseYn(code, user.getUserNo(), useYn);
    		
    		
    		if (deviceList == null || deviceList.size() <= 0) {
    			resultMap.put("result", "success");
    			resultMap.put("deviceCnt", 0);
    			return resultMap;
    		}
    		
    		//api 전송
    		Map<String, Object> params = new HashMap<String, Object>();
    		//String[] edgeList = deviceList.get().stream().map(d -> d.getEdgeId()).collect(Collectors.toList());
    		
    		String[] edgeList = deviceList.stream().map(d -> d.getEdgeId()).collect(Collectors.toList()).toArray(new String[deviceList.size()]);
    		params.put("edge_id_list", edgeList);
    		Map<String, Object> result = restUtil.post(Api.GET_EDGE_STATUS, params);
    		
    		String apiResult = result.get("status") == null ? "" : (String) result.get("status");
    		
    		if (result != null && "ok".equals(apiResult.toLowerCase())) {
    			
    			Map<String, Object> statusMap = (Map<String, Object>) result.get("edges_device_status");
            	
            	int normalCnt = 0;
            	int abnormalCnt = 0;
            	int errorCnt = 0;
            	
            	Set<String> keys = statusMap.keySet();
            	
            	for (String key : keys) {
            		Map<String, Object> edgeMap = (Map<String, Object>) statusMap.get(key);
            		String status = String.valueOf(edgeMap.get("edge_ai_status"));
            		switch (status) {
        			case "Working":
        				normalCnt++;
        				break;
        			case "Abnormal":
        				abnormalCnt++;
        				break;
        			case "Notworking":
        				errorCnt++;
        				break;
        			}
            		
            	}
            	
            	resultMap.put("result", "success");
            	resultMap.put("deviceCnt", deviceList.size());
            	resultMap.put("normalCnt", normalCnt);
            	resultMap.put("abnormalCnt", abnormalCnt);
            	resultMap.put("errorCnt", errorCnt);
            	resultMap.put("notCnt", deviceList.size() - keys.size());
            	
    		} else {
    			resultMap.put("result", "fail");
    			resultMap.put("msg", (result.get("error_cause") != null) ? result.get("error_cause") : result.get("error"));
    		}
    		
    	} catch(Exception e) {
    		resultMap.put("result", "fail");
    		resultMap.put("msg", e.getMessage());
    	}
    	
    	return resultMap;
    	
    }

    /**
     * 신규 고객문의 등록
     * @param customerServiceRequest
     * @return
     */
    public boolean setCustomerServiceRequest(CustomerServiceRequest customerServiceRequest, String serverName){
		//Optional.ofNullable(serviceRequestRepository.save(customerServiceRequest)).isPresent()
        Map<String, Object> map = new HashMap<>();
        map.put("fromaddr", customerServiceRequest.getEmail());
        map.put("toaddr", Email.TO_EMAIL.getMail());
        map.put("subject", "Edge Analysis 문의하기");
        
        String message = Email.SERVICE_TEMPLATE.getMail();
        
        message = message.replace("{name}", customerServiceRequest.getName())
        				 .replace("{email}", customerServiceRequest.getEmail())
        				 .replace("{tel}", customerServiceRequest.getTel())
        				 .replace("{server}", serverName)
        				 .replace("{content}", customerServiceRequest.getContent().replaceAll("\\n", "<br />"));
        
        map.put("message", message);
        
        Map<String, Object> result = restUtil.post(Api.SEND_MAIL, map);
        if(result != null && result.get("code").equals(200)) {
        	return true;
        }else {
        	return false;
        }
    }

    /**
     * 신규 견적문의 등록
     * @param customerQuoteRequest
     * @return
     */
    public boolean setCustomerQuoteRequest(CustomerQuoteRequest customerQuoteRequest, String serverName){
		//Optional.of(quoteRequestRepository.save(customerQuoteRequest)).isPresent()
        Map<String, Object> map = new HashMap<>();
        map.put("fromaddr", customerQuoteRequest.getEmail());
        map.put("toaddr", Email.TO_EMAIL.getMail());
        map.put("subject", "Edge Analysis 견적문의하기");
        
        String deviceName = CodeGroup.EDGE_GROUP.getCodeList().stream()
				.filter(c -> c.getCode().toLowerCase().equals(customerQuoteRequest.getEdgeType().toLowerCase())).findAny().orElse(Code.NOTCONNECT).getCodeName();
        String message = Email.SERVICE_QUOTE_TEMPLATE.getMail();
        
        message = message.replace("{name}", customerQuoteRequest.getName())
        				 .replace("{edge}", deviceName+"("+customerQuoteRequest.getEdgeType()+")")
        				 .replace("{email}", customerQuoteRequest.getEmail())
        				 .replace("{comp}", customerQuoteRequest.getCompanyNm())
        				 .replace("{rank}", customerQuoteRequest.getRank())
        				 .replace("{tel}", customerQuoteRequest.getTel())
        				 .replace("{server}", serverName);
        
        map.put("message", message);
        
        Map<String, Object> result = restUtil.post(Api.SEND_MAIL, map);
        if(result != null && result.get("code").equals(200)) {
        	return true;
        }else {
        	return false;
        }
    }

}
