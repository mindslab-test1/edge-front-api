package ai.maum.edge.frontapi.service;

import ai.maum.edge.frontapi.commons.security.JwtTokenProvider;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.model.TokenVO;
import ai.maum.edge.frontapi.model.entity.User;
import ai.maum.edge.frontapi.repository.RoleRepository;
import ai.maum.edge.frontapi.repository.UserRepository;
import ai.maum.edge.frontapi.model.entity.Role;
import ai.maum.edge.frontapi.model.entity.RoleName;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class AuthService {

    private final AuthenticationManager authManager;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final SSOInterface ssoIf;
    private final UserRepository userRepo;
    private final RoleRepository roleRepo;
    
//    @Transactional
    public Authentication authenticate(String id, String password)
    {
        Authentication auth = authManager.authenticate(new UsernamePasswordAuthenticationToken(id, password));
        SecurityContextHolder.getContext().setAuthentication(auth);
        return auth;
    }
    
    
    /*
     ** 인증 서버로부터 AccessToken(유효시간 포함)과 RefreshToken(유효시간 포함)을 발급받는다.
     */
    public TokenVO getAuthToken(String code, String requestUri) {
    	return ssoIf.publishTokens(code, requestUri);
    }
    
    /*
     ** 인증 서버로부터 AccessToken을 재발급 받는다
     */
    public TokenVO getRepublishTokens(String refreshToken) {
    	return ssoIf.republishTokens(refreshToken);
    }

    public User getUserInfo(TokenVO token) {

        if(token != null) {
            User user = userRepo.findByEmailAndUseYn(token.getEmail(), "Y");
            if (user == null) {
                User newUser = new User();
                newUser.setEmail(token.getEmail());
                newUser.setName(token.getName());
                newUser.setPassword("1234");
                //newUser.setApiId(token.getApiId());
                //newUser.setApiKey(token.getApiKey());
                newUser.setTel(token.getPhone());
                
                Role userRole = roleRepo.findByAuthName(RoleName.ROLE_USER.name());

                newUser.setRole(userRole);
                
                //newUser = userRepo.save(newUser);
                return newUser;
            } else {
                return user;
            }
        }
        else return null;
    }

    public String generateJwtToken(UserPrincipal userPrincipal, TokenVO token)
    {
        return jwtTokenProvider.generateToken(userPrincipal, token);
    }
    
    public String refreshGenerateToken(UserPrincipal userPrincipal, TokenVO token)
    {
        return jwtTokenProvider.generateRefreshToken(userPrincipal, token);
    }
    

    public String encode(String password)
    {
        return passwordEncoder.encode(password);
    }
}
