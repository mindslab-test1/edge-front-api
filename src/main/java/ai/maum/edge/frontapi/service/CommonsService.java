package ai.maum.edge.frontapi.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import ai.maum.edge.frontapi.commons.enums.CodeGroup;
import ai.maum.edge.frontapi.commons.security.UserPrincipal;
import ai.maum.edge.frontapi.commons.utils.ModelMapperService;
import ai.maum.edge.frontapi.model.dto.CodeDto;
import ai.maum.edge.frontapi.model.dto.MenuDto;
import ai.maum.edge.frontapi.model.dto.SideMenuDto;
import ai.maum.edge.frontapi.model.dto.SideMenuExInterface;
import ai.maum.edge.frontapi.model.entity.Menu;
import ai.maum.edge.frontapi.model.entity.User;
import ai.maum.edge.frontapi.repository.MenuRepository;
import ai.maum.edge.frontapi.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * CustomerService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2020-12-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2020-12-21
 */
@Service
public class CommonsService {

	Logger logger = LoggerFactory.getLogger(CommonsService.class);
	
	final MenuRepository menuRepository;
	
	final UserRepository userRepository;
	
	final EntityManager entityManager;
	
	final ModelMapperService modelMapperService;
	
    public CommonsService(ModelMapperService modelMapperService, MenuRepository menuRepository, UserRepository userRepository, EntityManager entityManager) {
    	this.modelMapperService = modelMapperService;
        this.menuRepository = menuRepository;
        this.userRepository = userRepository;
        this.entityManager = entityManager;
    }
    
    /**
     * 엣지 리스트 정보 조회
     * @param
     * @return
     */
    public List<Menu> getAllMenuList() {
    	
    	return menuRepository.findAll();
    	
    }
    
    public List<MenuDto> getHeaderMenuList(UserPrincipal user) {
    	Long usNo = user.getUserNo();
    	String compId = "";
    	if(usNo != null && usNo != 0) {
    		Optional<User> opUser = userRepository.findAllById(user.getUserNo());
    		if(opUser.isPresent()) {
    			User authUser = modelMapperService.map(opUser.get(), User.class);
    			compId = authUser.getClientCompanyId() == null || authUser.getClientCompanyId() == "" ? "" : authUser.getClientCompanyId();
    		}
    	}
    	
    	return menuRepository.getHeaderList(entityManager, user, compId);
    }
    
    public List<SideMenuDto> getSideMenuList(Long userId) {
    	
    	List<SideMenuExInterface> list = menuRepository.getSideMenuList(userId);
    	
    	if (Optional.ofNullable(list).isPresent()) {
    		return list.stream().map(menu -> new SideMenuDto(menu)).collect(Collectors.toList());    		
    	} else {
    		return null;
    	}
    	
    }
    
    public Map<String, Object> getCodeLists(List<String> codes) {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	for (String code : codes) {
    		CodeGroup group = Optional.ofNullable(CodeGroup.getCodeGroup(code)).orElse(CodeGroup.EMPTY);
        	
        	List<CodeDto> list = group.getCodeList().stream().map(c -> new CodeDto(c)).collect(Collectors.toList());
        	
        	map.put(code, list);
    	}
    	
    	return map;
    }
}
