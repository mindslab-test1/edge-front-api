package ai.maum.edge.frontapi.config;

import java.util.Map;

import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Service
public class RedisSubscriber {
	
	private final ObjectMapper objectMapper;
	
	private final SimpMessageSendingOperations messageTemplate;
	
	public void sendMessage(String message) {
		try {
			Map<String, Object> result = objectMapper.readValue(message, Map.class);
			messageTemplate.convertAndSend("/edge/" + (String) result.get("edgeId"), result.get("data"));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

}
