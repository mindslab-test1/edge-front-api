package ai.maum.edge.frontapi.commons.payload;

import lombok.*;

@Getter
@AllArgsConstructor
public class ApiResponse {

    private Boolean success;
    private String message;
    private Object data;
    
}
