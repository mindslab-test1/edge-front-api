package ai.maum.edge.frontapi.commons.utils;

import java.security.SecureRandom;

public class SerialCreator {

    final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    final SecureRandom rnd = new SecureRandom();

    private static SerialCreator instance;

    public static SerialCreator getInstance()
    {
        if(instance == null) instance = new SerialCreator();
        return instance;
    }

    public String create(int length)
    {
        StringBuilder sb = new StringBuilder( length );
        for( int i = 0; i < length; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

    public String createNumber(int length)
    {
        StringBuilder sb = new StringBuilder(length);
        for(int i = 0; i < length; i++) {
            sb.append(rnd.nextInt(10));
        }

        return sb.toString();
    }
}
