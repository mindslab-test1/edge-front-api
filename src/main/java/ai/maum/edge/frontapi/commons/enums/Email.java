package ai.maum.edge.frontapi.commons.enums;

import lombok.Getter;

@Getter
public enum Email {
	
	TO_EMAIL("hello@mindslab.ai", "받는 이메일 주소"),
	SERVICE_TEMPLATE("<div style='color : black'>이름 : {name}<br /><br />이메일 : {email}<br /><br />연락처 : {tel}<br /><br />문의 내용 : <br /><br />{content}<br /><br />* {server} 에서 보낸 메일입니다.</div>","문의하기 탬플릿"),
	SERVICE_QUOTE_TEMPLATE("<div style='color : black'>엣지 종류 : {edge}<br /><br />이름 : {name}<br /><br />회사 : {comp}<br /><br />직급 : {rank}<br /><br />연락처 : {tel}<br /><br />이메일 : {email}<br /><br />* {server} 에서 보낸 메일입니다.</div>","문의하기 탬플릿");
	
	private final String mail;
	private final String desc;
	
	Email (String mail, String desc) {
		this.mail = mail;
		this.desc = desc;
	}
	
}
