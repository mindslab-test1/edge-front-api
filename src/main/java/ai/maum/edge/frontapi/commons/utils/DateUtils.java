package ai.maum.edge.frontapi.commons.utils;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.EnumSet;

public class DateUtils {

    private static DateUtils instance;

    public static DateUtils getInstance()
    {
        if(instance == null) {
            instance = new DateUtils();
        }
        return instance;
    }

    private DateUtils(){}

//    public boolean isTodayWeekday()
//    {
//        Date date = new Date();
//
//        Calendar cal = Calendar.getInstance();
//        cal.setTime(date);
//
//        return (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) && (cal.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY);
//    }

    public boolean isTodayWeekday()
    {
        return EnumSet.of(DayOfWeek.MONDAY, DayOfWeek.FRIDAY).contains(LocalDate.now(ZoneId.of("Asia/Seoul")).getDayOfWeek());
    }

    public boolean isNowBetweenTwoLocalTime(LocalTime open, LocalTime close)
    {
        LocalTime now = LocalTime.now();
        return open.isBefore(now) && close.isAfter(now);
    }

}
