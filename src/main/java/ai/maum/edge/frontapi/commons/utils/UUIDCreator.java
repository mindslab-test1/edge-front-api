package ai.maum.edge.frontapi.commons.utils;

import java.util.UUID;

public class UUIDCreator {

    private static UUIDCreator instance;

    private UUIDCreator(){}

    public static UUIDCreator getInstance()
    {
        if(instance == null) instance = new UUIDCreator();
        return instance;
    }

    public String createRandom() { return UUID.randomUUID().toString(); }

    public String createUserId() { return "U" + createRandom(); }

    public String createOrderId() { return "O" + createRandom(); }

    public String createPresentId() { return "P" + createRandom(); }

    public String createImageFileId() { return "I" + createRandom(); }

    public String createChatMeetingRoomId() { return "C" + createRandom(); }
}
