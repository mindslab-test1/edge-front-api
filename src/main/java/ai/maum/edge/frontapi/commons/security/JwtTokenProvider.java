package ai.maum.edge.frontapi.commons.security;

import ai.maum.edge.frontapi.model.TokenVO;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import ai.maum.edge.frontapi.commons.utils.CookieUtil;
import ai.maum.edge.frontapi.repository.DeviceRepository;
import ai.maum.edge.frontapi.repository.MenuRepository;
import ai.maum.edge.frontapi.repository.UserRepository;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.EntityManager;

@Slf4j
@Component
public class JwtTokenProvider {

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private long jwtExpirationInMs;
    
    @Value("${app.jwtRefreshExpriationInMs}")
    private long jwtRefreshExpriationInMs;
    
    @Autowired
    private DeviceRepository deviceRepo;
    
    @Autowired
    private MenuRepository menuRepository;
    
    @Autowired
    private EntityManager entityManager;
    
    @Autowired
    private UserRepository userRepository;
    
    public String generateToken(UserPrincipal userPrincipal, TokenVO token)
    {
    	
        Date expiryDate = null;
        
		/*
		 * Date now = new Date(); expiryDate = new Date(now.getTime() + 40000);
		 */
        /*Date now = new Date(); 
        expiryDate = new Date(now.getTime() + 30000);*/
        try {
            expiryDate = Timestamp.valueOf(LocalDateTime.parse(token.getAccess_expire_time()));
        } catch (Exception e) {
        	Date now = new Date();        
        	expiryDate = new Date(now.getTime() + jwtExpirationInMs);        	
        }

        return Jwts.builder()
                .setSubject(Long.toString(userPrincipal.getUserNo()))
                .setIssuedAt(new Date())
                .claim("name", userPrincipal.getUsername())
                .claim("email", userPrincipal.getEmail())
                .claim("accessToken", token.getAccess_token())
                .claim("uuid", token.getUuid())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }
    
    public String generateRefreshToken(UserPrincipal userPrincipal, TokenVO token)
    {
        Date expiryDate = null;
        
        try {
            expiryDate = Timestamp.valueOf(LocalDateTime.parse(token.getRefresh_expire_time()));
        } catch (Exception e) {
        	Date now = new Date();        
        	expiryDate = new Date(now.getTime() + jwtRefreshExpriationInMs);        	
        }

        return Jwts.builder()
                .setSubject(Long.toString(userPrincipal.getUserNo()))
                .setIssuedAt(new Date())
                .claim("name", userPrincipal.getUsername())
                .claim("email", userPrincipal.getEmail())
                .claim(CookieUtil.REFRESH_TOKEN, token.getRefresh_token())
                .claim("uuid", token.getUuid())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public Long getUserNoFromJWT(String token)
    {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return Long.valueOf(claims.getSubject());
    }
    
    public Object getClaimFromJWT(String token, String key) {
    	Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();
    	
    	return claims.get(key);
    }

    public boolean validateToken(String authToken)
    {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            log.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        }
        return false;
    }
}
