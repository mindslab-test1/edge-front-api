package ai.maum.edge.frontapi.commons.utils;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ModelMapperService {

    private final ModelMapper modelMapper;

    ModelMapperService()
    {
        this.modelMapper = new ModelMapper();
    }

    public <D, T> D map(final T entity, Class<D> outClass)
    {
        modelMapper.getConfiguration().setAmbiguityIgnored(false);
        return modelMapper.map(entity, outClass);
    }

    public <D, T> D mapAmbiguityIgnored(final T entity, Class<D> outClass)
    {
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        return modelMapper.map(entity, outClass);
    }

    public <D, T> List<D> mapAll(final Collection<T> entityList, Class<D> outClass)
    {
        return entityList.stream()
                .map(entity -> map(entity, outClass))
                .collect(Collectors.toList());
    }
    
    public <T> T mapToClass(Map<String, Object> map, Class<T> outClass) {
    	T instance = null;
    	try {
    		instance = outClass.getDeclaredConstructor().newInstance();
        	
        	Field[] f = instance.getClass().getDeclaredFields();
        	
        	for (int i = 0; i < f.length; i++) {
        		f[i].setAccessible(true);
        		
        		try {
        			Object value = map.get(f[i].getName());
        			f[i].set(instance, value);
        		} catch (Exception e) {
    				e.printStackTrace();
    			}
        		
        	}
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    	return instance;
    }
}
