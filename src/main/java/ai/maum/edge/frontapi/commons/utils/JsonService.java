package ai.maum.edge.frontapi.commons.utils;

import ai.maum.edge.frontapi.commons.exception.StackTrace;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class JsonService {

    private final ObjectMapper objectMapper;

    public JsonService()
    {
        this.objectMapper = new ObjectMapper();
    }

    public String objectToJson(Object obj)
    {
        try {
            return objectMapper.writeValueAsString(obj);
        }
        catch (JsonProcessingException jpe) {
            log.warn("ObjectToJson JsonProcessingException=" + StackTrace.getStackTrace(jpe));
            return null;
        }
    }
}
