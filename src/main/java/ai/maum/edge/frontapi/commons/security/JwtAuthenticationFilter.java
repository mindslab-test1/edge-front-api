package ai.maum.edge.frontapi.commons.security;


import ai.maum.edge.frontapi.commons.utils.CookieUtil;
import ai.maum.edge.frontapi.commons.utils.RedisUtil;
import ai.maum.edge.frontapi.model.TokenVO;
import ai.maum.edge.frontapi.model.dto.SideMenuDto;
import ai.maum.edge.frontapi.service.AuthService;
import ai.maum.edge.frontapi.service.UserSecurityDetailService;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired private JwtTokenProvider tokenProvider;
    @Autowired private UserSecurityDetailService userDetailSvc;
    @Autowired private RedisUtil redisUtil;
    @Autowired private CookieUtil cookieUtil;
    @Autowired private AuthService authSvc;
    
    @Value("${app.jwtRefreshExpriationInMs}")
    private long jwtRefreshExpriationInMs;


    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain filterChain) throws ServletException, IOException {
        
    	String refreshJwt = null;
    	boolean isAuthToken = false;
    	Long userId = null;
    	String jwt = null;
    	
    	if (req.getRequestURI().equals("/api/auth/user/logout")) {
        	filterChain.doFilter(req, res);
        	return;
        }
    	
    	try {
            jwt = getJwtFromRequest(req);
            
            if(StringUtils.hasText(jwt)) {
                userId = tokenProvider.getUserNoFromJWT(jwt);
                isAuthToken = true;
            }
            
        }
        catch (ExpiredJwtException e) {
        	Cookie cookie = cookieUtil.getCookie(req, CookieUtil.REFRESH_TOKEN);
        	
        	if (cookie != null) {
        		refreshJwt = cookie.getValue();
        	} else {
        		//throw new ExpiredJwtException(null, null, "expired");
        		setErrorResponse(HttpStatus.UNAUTHORIZED, res, "expired");
        		return;
        	}
        	
        } catch (Exception e) {
			// TODO: handle exception
		}
    	
    	try {
    		
    		if (refreshJwt != null) {
    			
    			userId = tokenProvider.getUserNoFromJWT(refreshJwt);
    			String saveRefreshToken = redisUtil.getData(CookieUtil.REFRESH_TOKEN + "_" + userId);
    			
            	// refreshToken이 변조되지 않았거나 서버 재시작등의 이유로 저장된 refreshtoken이 없는 경우
            	// 재발급 진행
    			if (refreshJwt.equals(saveRefreshToken) || saveRefreshToken == null || "".equals(saveRefreshToken)) {
    				String ssoRefreshToken = (String) tokenProvider.getClaimFromJWT(refreshJwt, CookieUtil.REFRESH_TOKEN);
    				TokenVO token = authSvc.getRepublishTokens(ssoRefreshToken);
    				
    				if (token != null) {
    					token.setUuid((String) tokenProvider.getClaimFromJWT(refreshJwt, "uuid"));
    					UserPrincipal user = (UserPrincipal) userDetailSvc.loadUserById(userId);
    					
    	    			jwt = authSvc.generateJwtToken(user, token);
    	    			String newRefreshToken = authSvc.refreshGenerateToken(user, token);
    	    			
    	    			int maxAge = (int) (jwtRefreshExpriationInMs/1000);
    	    			Cookie accessCookie = cookieUtil.setCookie("userToken", jwt, maxAge);
    	    			Cookie refreshCookie = cookieUtil.setCookie(CookieUtil.REFRESH_TOKEN, newRefreshToken, maxAge);
    	            	res.addCookie(refreshCookie);
    	            	res.addCookie(accessCookie);
    	            	
    	            	redisUtil.setData(CookieUtil.REFRESH_TOKEN + "_" + userId, newRefreshToken);
    	            	isAuthToken = true;
    	    		}
    				
    			}
    			
    		}
    		
    	} catch (Exception e) {
    		
    	}
    	
    	try {
    		
    		if (isAuthToken) {
        		String refererURI = new URI(req.getHeader("referer")).getPath();
                boolean isAuthCheck = false;
                
                UserDetails userDetails = null;
                
                if (req.getRequestURL().indexOf("/auth/user") > -1) {
                	isAuthCheck = true;
                }
                
                if (userId == 0 && ("/home".equals(refererURI) || isAuthCheck)) {
                	userDetails = UserPrincipal.create(userId, String.valueOf(tokenProvider.getClaimFromJWT(jwt, "email")), String.valueOf(tokenProvider.getClaimFromJWT(jwt, "name")));
                	isAuthCheck = true;
                } else {
                	userDetails = userDetailSvc.loadUserById(userId);
                	
                	//중복 로그인 체크
                	if (!duplicateLoginCheck(jwt, userId)) {
                		setErrorResponse(HttpStatus.UNAUTHORIZED, res, "duplicateLogin");
                		return;
                	}
                	
                	
                	//메뉴 권한 체크
                	if (!isAuthCheck) {
                		
                		List<SideMenuDto> menuList = userDetailSvc.getMenuByUserId(userId);
                		if(menuList != null) {
                			isAuthCheck = menuList.stream().anyMatch(menu -> refererURI.equals(menu.getUrl()));
                    	}
                	}
                	
                }
                
                if (isAuthCheck) {
                	UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));

                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
        	} 
    		
    	} catch (Exception e) {
    		
    	}

        filterChain.doFilter(req, res);
    }

    private String getJwtFromRequest(HttpServletRequest req)
    {
        String bearerToken = req.getHeader("Authorization");

        if(StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
//            return bearerToken.substring(7, bearerToken.length());
            return bearerToken.substring(7);
        }

        return null;
    }
    
    //중복 로그인 체크
    public boolean duplicateLoginCheck(String jwt, Long userId) {
    	String loginUid = redisUtil.getData("userId_" + userId);
    	String uuid = (String) tokenProvider.getClaimFromJWT(jwt, "uuid");    		
    	    	
    	if (loginUid != null && !"".equals(loginUid)) {
    		if (!loginUid.equals(uuid)) {
    			return false;
    		}
    		
    	} else {
    		redisUtil.setData("userId_" + userId, uuid);
    	}
    	
    	return true;
    }
    
    //에러 전송
    public void setErrorResponse(HttpStatus status, HttpServletResponse response, String msg){
        response.setStatus(status.value());
        response.setContentType("application/json");
        try {
        	
        	JSONObject jsonObj = new JSONObject();
        	jsonObj.put("status", status.value());
        	jsonObj.put("message", msg);
        	jsonObj.put("timestamp", LocalDateTime.now() + "");
        	
            String json = jsonObj.toJSONString();
            System.out.println(json);
            response.getWriter().write(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
}
