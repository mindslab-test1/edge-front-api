package ai.maum.edge.frontapi.commons.utils;

import javax.servlet.http.HttpServletRequest;

public class CommonUtils {

    public static String getImageExtFromBase64(String base64)
    {
        String[] strings = base64.split(",");
        if ("data:image/png;base64".equals(strings[0])) {
            return "png";
        }
        else if ("data:image/jpeg;base64".equals(strings[0])) {
            return "jpeg";
        }
        return "jpg";
    }
    
	public static String getRequestIp(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		
		if (ip == null) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null) {
			ip = request.getHeader("WL-Proxy-Client-IP"); // 웹로직
		}
		if (ip == null) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (ip == null) {
			ip = request.getRemoteAddr();
		}
		
		return ip;
	}
}
