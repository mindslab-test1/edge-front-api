package ai.maum.edge.frontapi.commons.security;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ai.maum.edge.frontapi.model.entity.RoleName;
import ai.maum.edge.frontapi.model.entity.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class UserPrincipal implements UserDetails {

    @Getter @Setter
    private Long userNo;

    @Setter
    private String name;

    @JsonIgnore @Getter
    private final String email;

    @JsonIgnore
    private final String password;

    private User user;

    private Collection<? extends GrantedAuthority> authorities;

    public UserPrincipal(Long userNo, String name, String email, String password, Collection<? extends GrantedAuthority> authorities)
    {
        this.userNo = userNo;
        this.name = name;
        this.email = email;
        this.password = password;
        this.authorities = authorities;
    }
    
    public static UserPrincipal create(Long userId, String email, String name)
    {
		User dummy = new User();
		dummy.setEmail(email);
		dummy.setId(userId);
		dummy.setName(name);
		List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(RoleName.ROLE_USER.name()));
		return new UserPrincipal(dummy.getId(), dummy.getName(), dummy.getEmail(), dummy.getPassword(), authorities);
        
    }

    public static UserPrincipal create(User user)
    {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(user.getRole().getAuthName()));
        return new UserPrincipal(user.getId(), user.getName(), user.getEmail(), user.getPassword(), authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public User getUser()
    {
        if(this.user == null) {
            this.user = new User();
            this.user.setId(userNo);
            this.user.setEmail(email);
            this.user.setName(name);
        }
        return this.user;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(userNo, that.userNo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userNo);
    }
}
