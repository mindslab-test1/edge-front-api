package ai.maum.edge.frontapi;

import java.time.*;
import java.util.HashMap;
import java.util.Map;

public class TestClass {

    public static void main(String[] args)
    {
//        LocalDateTime nowFrom = LocalDateTime.of(LocalDate.now(), LocalTime.MIN);
//        LocalDateTime nowTo = LocalDateTime.of(LocalDate.now(), LocalTime.MAX);
//        System.out.println(nowFrom);
//        System.out.println(nowTo);

        String yearMonth = "2020-09";

        int year = Integer.parseInt(yearMonth.substring(0, 4));
        int month = Integer.parseInt(yearMonth.substring(5, 7));

        System.out.println(year);
        System.out.println(month);
    }
}
